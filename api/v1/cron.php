<?php
set_time_limit(0);
date_default_timezone_set('Asia/Singapore');

class Process
{
    private $pid;
    private $command;

    public function __construct($cl = false)
    {
        if ($cl != false) {
            $this->command = $cl;
            $this->runCom();
        }
    }
    private function runCom()
    {
        $command = 'nohup ' . $this->command . ' > /dev/null 2>&1 & echo $!';
        exec($command, $op);
        $this->pid = (int)$op[0];
    }

    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function status()
    {
        $command = 'ps -p ' . $this->pid;
        exec($command, $op);
        if (!isset($op[1])) return false;
        else return true;
    }

    public function start()
    {
        if ($this->command != '') $this->runCom();
        else return true;
    }

    public function stop()
    {
        $command = 'kill ' . $this->pid;
        exec($command);
        if ($this->status() == false) return true;
        else return false;
    }
}

$live = false;
$staging = false;

if ($live) {
    $db_user= "pikohana_dba";
    $db_pass= "HPAKT6Ve";
    $db_name= "pikohana_prd";
    $db_host= "pikohana-prd.cqolviivmhgq.ap-southeast-1.rds.amazonaws.com";
    $base_url = "https://www.pikohana.com";
}
else if ($staging) {
    $db_user= "pikohana_dba";
    $db_pass= "HPAKT6Ve";
    $db_name= "pikohana_stage";
    $db_host= "pikohana-prd.cqolviivmhgq.ap-southeast-1.rds.amazonaws.com";
    $base_url = "https://www.pikohana.com/beta";
}

try {
    $conn = new PDO("mysql:host=$db_host;dbname=".$db_name, $db_user, $db_pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$sql = "SELECT * FROM client_oauth_token WHERE client_master_id NOT IN(59,60)";
$stmt =  $conn->query($sql);
$clients = $stmt->fetchAll();

foreach ($clients as $row) {
    $cmd= "curl '". $base_url . "/api/v1/client/xero/refresh?authenticate=0&page_name=plreport&client_id=" . $row['client_id'] . "&refresh=1&client_master_id=" . $row['client_master_id']."&pid=true'";
    $start_time = date('Y-m-d H:i:s');
    $client_id = $row['client_id'];
    $client_master_id = $row['client_master_id'];
    $process = new Process($cmd);
    $pid= $process->getPid();
    $data = [
        'cmd' => $cmd,
        'start_time' => $start_time,
        'client_master_id' => $client_master_id,
        'client_id' => $client_id,
        'status'=> 0,
        'pid'=> $pid
    ];
    $isql = "INSERT INTO cron_jobs (cmd,start_time,client_master_id,client_id,status,pid) VALUES (:cmd, :start_time, :client_master_id, :client_id, :status, :pid)";
    $stm = $conn->prepare($isql);
    $stm->execute($data);
}

?>