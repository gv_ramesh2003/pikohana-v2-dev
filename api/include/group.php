<?php

require_once dirname(__FILE__) . '/DbConnect.php';

class group extends utility
{
    // private $conn;
    public $db;

    public function __construct()
    {
        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function InsertGroupDetails($group_name, $admin_id, $report_start_date, $default_currency, $selectedClients)
    {
        $response = array();
        $getGroupId = 0;
        try
        {
            $sql = "SELECT client_master_id FROM client_master_detail WHERE company_name = ? ";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("s", $group_name);
                $stmt->execute();
                $stmt->bind_result($client_master_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $response["error"] = true;
                    $response["message"] = GROUP_NAME_EXIST;
                } else {
                    $sql1 = "INSERT INTO client_master_detail (company_name, report_start_date, default_currency, created_date, is_group, is_active, pdf_report_status) values(?, ?, ?, ?, ?, ?, ?)";

                    $is_group = 1;
                    $is_active = 1;
                    $pdf_report_status = 1;
                    if ($stmt1 = $this->conn->prepare($sql1)) {
                        $date = date("Y-m-d H:i:s");
                        $stmt1->bind_param("ssssiii", $group_name, $report_start_date, $default_currency, $date, $is_group, $is_active, $pdf_report_status);
                        $result = $stmt1->execute();
                        $getGroupId = $this->conn->insert_id;
                        $toCur = $default_currency;
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }

                    if ($getGroupId > 0) {
                        foreach ($selectedClients as $value) {
                            $sql = 'SELECT map_id FROM `master_groups_company_list` WHERE `group_id` = ? and `client_master_id` = ?';
                            if ($stmt = $this->conn->prepare($sql)) {
                                $selectedClientsID = $value['id'];
                                $stmt->bind_param('ii', $getGroupId, $selectedClientsID);
                                if ($stmt->execute()) {
                                    $stmt->store_result();
                                    $stmt->bind_result($map_id);
                                    $stmt->fetch();
                                    $num_rows = $stmt->num_rows;
                                    $stmt->close();
                                    if ($num_rows == 0) {
                                        $sql1 = 'INSERT INTO master_groups_company_list (group_id, client_master_id, created_by, created_date) values (?, ?, ?, ?)';
                                        if ($stmt = $this->conn->prepare($sql1)) {
                                            $date = date("Y-m-d h:i:s");
                                            $stmt->bind_param('iiis', $getGroupId, $selectedClientsID, $admin_id, $date);
                                            $result = $stmt->execute();
                                            $stmt->close();
                                            if ($result) {
                                                $this->conn->commit();
                                                $response['error'] = false;
                                                $response["message"] = CREATE_GROUP_SUCCESS;
                                                $dash_db = new dashboardReport();
                                                $master_group_sql = 'SELECT client_master_id, account_code, account_name,account_type, account_category,account_type_alias, account_order  FROM `master_companies_account_categories`  where `client_master_id` = ?';
                                                if ($master_group_stmt = $this->conn->prepare($master_group_sql)) {
                                                    $master_group_stmt->bind_param('i', $selectedClientsID);
                                                    if ($master_group_stmt->execute()) {
                                                        $master_group_stmt->store_result();
                                                        $master_group_stmt->bind_result($client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                                        $num_rows = $master_group_stmt->num_rows;

                                                        if ($num_rows > 0) {
                                                            while ($masterResult = $master_group_stmt->fetch()) {
                                                                $dash_db->insertGroupAccountCategories($getGroupId, $client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                $response['error'] = true;
                                                $response['message'] = QUERY_EXCEPTION;
                                            }
                                        } else {
                                            $response['error'] = true;
                                            $response['message'] = QUERY_EXCEPTION;
                                        }
                                    }
                                } else {
                                    $response['error'] = true;
                                    $response['message'] = QUERY_EXCEPTION;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = CREATE_GROUP_INVALID_CLIENT_ID;
                    }
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function GetListGroup()
    {
        $response = array();
        $is_active = 1;
        try
        {
            $sql = "SELECT DISTINCT cmd.client_master_id, cmd.company_name, count(cl.map_id) as count, count(cgm.group_mapping_id) as is_map
                    FROM client_master_detail cmd
                    JOIN master_groups_company_list as cl ON cl.group_id = cmd.client_master_id
                    LEFT JOIN client_group_mapping as cgm ON cgm.group_id = cmd.client_master_id
                    where cmd.is_group = 1  and cmd.is_group_deleted = 0
                    GROUP BY cmd.client_master_id, cmd.company_name, cgm.group_mapping_id
                    order by cmd.company_name asc";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->bind_result($group_id, $group_name, $count, $is_map);
                $stmt->store_result();
                $pushArr = array();
                if ($stmt->num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        if ($is_map > 0) {
                            $is_mapped = 1;
                        } else {
                            $is_mapped = 0;
                        }
                        $pushArr[] = array('group_name' => $group_name, 'group_id' => $group_id, 'company_count' => $count, 'is_mapped' => $is_mapped);
                    }
                    $response["error"] = false;
                    $response['result'] = $pushArr;
                } else {
                    $response["error"] = true;
                    $response['result'] = NO_COMPANY_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function GetGroupCompanyList($group_id)
    {
        $response = array();
        $is_active = 1;
        try
        {
            $sql = " SELECT  cl.client_master_id,cd.company_name,cmd.company_name as group_name,cmd.client_master_id as group_id, cmd.company_logo as group_logo, cmd.report_start_date,
            		cmd.default_currency, cmd.last_updated_date, cmd.created_date
        		FROM master_groups_company_list cl
                        JOIN client_master_detail cmd ON cmd.client_master_id = cl.group_id
                        JOIN client_master_detail cd ON cd.client_master_id = cl.client_master_id AND cd.is_active = ?
                        WHERE cl.group_id = ? ";
            if ($stmte = $this->conn->prepare($sql)) {
                $stmte->bind_param("ii", $is_active, $group_id);
                $stmte->execute();
                $stmte->bind_result($client_master_id, $company_name, $group_name, $group_id, $group_logo, $report_start_date, $default_currency, $updated_date, $created_date);
                $stmte->store_result();
                if ($stmte->num_rows > 0) {
                    $temp = array();
                    $response["error"] = false;
                    while ($compResult = $stmte->fetch()) {
                        $company_details .= $company_name . ', ';
                        $temp[] = array('client_master_id' => $client_master_id, "company_name" => "$company_name");
                    }
                    $response["group_name"] = $group_name;
                    $response["group_id"] = $group_id;
                    if ($group_logo != '' && $group_logo != null) {
                        $logo = $group_logo;
                    } else {
                        $logo = "";
                    }

                    if ($default_currency != '' && $default_currency != null) {
                        $currency = $default_currency;
                    } else {
                        $currency = "";
                    }
                    $response["group_logo"] = ($logo == null || $logo == "") ? "media/group/nologo.png" : $logo;
                    $response["report_start_date"] = $report_start_date;
                    $response["default_currency"] = $currency;
                    if ($updated_date != "" && $updated_date != null) {
                        $response["updated_date"] = date("dS F Y", strtotime($updated_date));
                    } else {
                        $response["updated_date"] = date("dS F Y", strtotime($created_date));
                    }
                    $response["company_name"] = substr($company_details, 0, -2);
                    $response["clients"] = $temp;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_GROUP_DETAILS_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateGroupDetails($group_id, $group_name, $admin_id, $selectedClients, $deletedClients, $default_currency)
    {
        $dash_db = new dashboardReport();
        $response = array();
        $getGroupId = 0;
        $report_start_date = "";
        try
        {
            $toCur = $default_currency;
            $sql = "SELECT client_master_id, report_start_date FROM client_master_detail WHERE client_master_id != ? AND company_name = ? ";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $group_id, $group_name);
                $stmt->execute();
                $stmt->bind_result($client_master_id, $report_start_date);
                $stmt->store_result();
                if ($stmt->num_rows == 0) {
                    $sql = "UPDATE client_master_detail SET default_currency = ? WHERE client_master_id = ?";
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("si", $default_currency, $group_id);
                        $result = $stmt->execute();
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = GROUP_NAME_EXIST;
                    return $response;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
                return response;
            }

            $groupIdsql = "SELECT  report_start_date FROM client_master_detail WHERE client_master_id = ?";
            if ($stmt = $this->conn->prepare($groupIdsql)) {
                $stmt->bind_param("i", $group_id);
                $stmt->execute();
                $stmt->bind_result($report_start_date);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    while ($masterResult = $stmt->fetch()) {
                        $report_start_date = $report_start_date;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = GROUP_NAME_EXIST;
                    return $response;
                }
            }

            /* If Group name validate then only update will happen*/
            if (sizeof($deletedClients) > 0) {
                $this->hardDeleteMasterCompany($group_id, $deletedClients);
                $this->hardDeleteGroupCompanyCategories($group_id, $deletedClients);
                $response['error'] = false;
                $response['message'] = GROUP_DELETE_SUCCESS;
            }
            if (sizeof($selectedClients) > 0) {
                $name = array();
                foreach ($selectedClients as $value) {
                    $client_master_id = $value['id'];
                    $getAccountName = $dash_db->getEliMinateNameByID($client_master_id);
                    if ($getAccountName['error'] == false) {
                        $name[] = $getAccountName;
                    }
                }

                foreach ($selectedClients as $value) {
                    $client_master_id = $value['id'];
                    $dash_db->deleteGroupClientData($client_master_id, $group_id);
                }

                $this->storeByselectedCompany($group_id, $admin_id, $selectedClients);
                $this->UpdateGroupCompanyDetails($group_id, $admin_id, $selectedClients);
                $this->updateEliminateByName($name);
                $response['error'] = false;
                $response['message'] = CREATE_GROUP_UPDATE_SUCCESS;
            } else {
                $response["error"] = true;
                $response["error"] = CREATE_GROUP_INVALID_CLIENT_ID;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hardDeleteMasterCompany($group_id, $deletedClients)
    {
        try {
            foreach ($deletedClients as $value) {
                $sql = 'DELETE FROM `master_groups_company_list` WHERE `group_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $deletedClientsID = $value['id'];
                    $stmt->bind_param('ii', $group_id, $deletedClientsID);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response['error'] = false;
                        $response['error'] = $result;
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hardDeleteGroupCompanyCategories($group_id, $deletedClients)
    {
        try {
            foreach ($deletedClients as $value) {
                $sql = 'DELETE FROM `group_companies_account_categories` WHERE `group_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $deletedClientsID = $value['id'];
                    $stmt->bind_param('ii', $group_id, $deletedClientsID);
                    $result = $stmt->execute();
                    $this->conn->commit();
                    $stmt->close();
                    if ($result) {
                        $this->conn->commit();
                        $response['error'] = false;
                        $response['error'] = $result;
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function storeByselectedCompany($group_id, $admin_id, $selectedClients)
    {
        try {
            foreach ($selectedClients as $value) {
                $sql = 'SELECT * FROM `master_groups_company_list` WHERE `group_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $selectedClientsID = $value['id'];
                    $stmt->bind_param('ii', $group_id, $selectedClientsID);
                    if ($stmt->execute()) {
                        $stmt->store_result();
                        $stmt->bind_result($map_id);
                        $stmt->fetch();
                        $num_rows = $stmt->num_rows;
                        $stmt->close();
                        if ($num_rows == 0) {
                            $date = date("Y-m-d h:i:s");
                            $sql1 = 'INSERT INTO master_groups_company_list(group_id,client_master_id, created_by, created_date ) values(?,?,?,?)'; //echo $sql1;exit;
                            if ($stmt = $this->conn->prepare($sql1)) {
                                $stmt->bind_param('iiis', $group_id, $selectedClientsID, $admin_id, $date);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $this->conn->commit();
                                    $response['error'] = false;
                                    $response['error'] = $result;
                                } else {
                                    $response['error'] = true;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        }
                    } else {
                        $response['error'] = true;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateGroupCompanyDetails($group_id, $admin_id, $selectedClients)
    {
        $response = array();
        try
        {
            foreach ($selectedClients as $value) {
                $sql = 'SELECT map_id FROM `master_groups_company_list` WHERE `group_id` = ? and `client_master_id` = ?';
                if ($stmt = $this->conn->prepare($sql)) {
                    $selectedClientsID = $value['id'];
                    $stmt->bind_param('ii', $group_id, $selectedClientsID);
                    if ($stmt->execute()) {
                        $stmt->store_result();
                        $stmt->bind_result($map_id);
                        $stmt->fetch();
                        $num_rows = $stmt->num_rows;
                        $stmt->close();
                        if ($num_rows == 0) {
                            $sql1 = 'INSERT INTO master_groups_company_list (group_id, client_master_id, created_by, created_date) values (?, ?, ?, ?)';
                            if ($stmt = $this->conn->prepare($sql1)) {
                                $date = date("Y-m-d h:i:s");
                                $stmt->bind_param('iiis', $group_id, $selectedClientsID, $admin_id, $date);
                                $result = $stmt->execute();
                                $stmt->close();
                                if ($result) {
                                    $this->conn->commit();
                                    $response['error'] = false;
                                    $response["message"] = CREATE_GROUP_SUCCESS;
                                    $dash_db = new dashboardReport();
                                    $master_group_sql = 'SELECT client_master_id, account_code, account_name,account_type, account_category,account_type_alias, account_order  FROM `master_companies_account_categories`  where `client_master_id` = ?';
                                    if ($master_group_stmt = $this->conn->prepare($master_group_sql)) {
                                        $master_group_stmt->bind_param('i', $selectedClientsID);
                                        if ($master_group_stmt->execute()) {
                                            $master_group_stmt->store_result();
                                            $master_group_stmt->bind_result($client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                            $num_rows = $master_group_stmt->num_rows;

                                            if ($num_rows > 0) {
                                                while ($masterResult = $master_group_stmt->fetch()) {
                                                    $dash_db->insertGroupAccountCategories($group_id, $client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $response['error'] = true;
                                    $response['message'] = QUERY_EXCEPTION;
                                }
                            } else {
                                $response['error'] = true;
                                $response['message'] = QUERY_EXCEPTION;
                            }
                        } else {
                            $dash_db = new dashboardReport();
                            $master_group_sql = 'SELECT client_master_id, account_code, account_name,account_type, account_category,account_type_alias, account_order  FROM `master_companies_account_categories`  where `client_master_id` = ?';
                            if ($master_group_stmt = $this->conn->prepare($master_group_sql)) {
                                $master_group_stmt->bind_param('i', $selectedClientsID);
                                if ($master_group_stmt->execute()) {
                                    $master_group_stmt->store_result();
                                    $master_group_stmt->bind_result($client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                    $num_rows = $master_group_stmt->num_rows;

                                    if ($num_rows > 0) {
                                        while ($masterResult = $master_group_stmt->fetch()) {
                                            $dash_db->insertGroupAccountCategories($group_id, $client_master_id, $account_code, $account_name, $account_type, $account_category, $account_type_alias, $account_order);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $response['error'] = true;
                        $response['message'] = QUERY_EXCEPTION;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = QUERY_EXCEPTION;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateGroupDateDetails($group_id, $report_start_date, $admin_id)
    {
        $response = array();
        $getGroupId = 0;
        try
        {
            $sql = "SELECT client_master_id FROM client_master_detail WHERE client_master_id = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $group_id);
                $stmt->execute();
                $stmt->bind_result($group_id);
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $date = date("Y-m-d H:i:s");
                    $sql = "UPDATE client_master_detail SET report_start_date = ?, last_updated_date = ?, last_updated_by = ? WHERE client_master_id = ?";
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("ssii", $report_start_date, $date, $admin_id, $group_id);
                        $result = $stmt->execute();
                    }
                    $response["error"] = false;
                    $response["message"] = GROUP_DATE_UPDATE;
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateGroupName($group_id, $group_name, $admin_id)
    {
        $response = array();
        $getGroupId = 0;
        try
        {
            $sql = "SELECT client_master_id as group_id FROM client_master_detail WHERE client_master_id != ? AND company_name = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("is", $group_id, $group_name);
                $stmt->execute();
                $stmt->bind_result($group_id);
                $stmt->store_result();
                if ($stmt->num_rows == 0) {
                    $date = date("Y-m-d H:i:s");
                    $sql = "UPDATE client_master_detail SET company_name = ?, last_updated_date = ?, last_updated_by = ? WHERE client_master_id = ?";
                    if ($stmt = $this->conn->prepare($sql)) {
                        $stmt->bind_param("ssii", $group_name, $date, $admin_id, $group_id);
                        $result = $stmt->execute();
                    }
                    $response["error"] = false;
                    $response["message"] = GROUP_NAME_UPDATE;
                } else {
                    $response["error"] = true;
                    $response["message"] = GROUP_NAME_EXIST;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function UpdateGroupLogo($group_id, $group_logo, $admin_id)
    {
        $response = array();
        $getGroupId = 0;
        try
        {
            if (isset($group_logo) && isset($group_logo['name'])) {
                $module = 'group';
                $upload_result = array();
                $utility = new utility;
                $upload_result = $utility->uploadImages($group_logo, $module, $group_id);

                if ($upload_result["error"] == false) {
                    $profile_url = $upload_result["message"];
                } else {
                    return $upload_result;
                }
            } else if (isset($group_logo)) {
                $profile_url = $group_logo;
            }

            $sql = "UPDATE client_master_detail SET company_logo = ?, last_updated_date = ?, last_updated_by = ? WHERE client_master_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ssii", $profile_url, $date, $admin_id, $group_id);
                $result = $stmt->execute();
                $response["error"] = false;
                $response['group_logo'] = $profile_url;
                $response["message"] = GROUP_LOGO_UPDATE;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getCategoryByGroupCompany($group_id, $client_master_id)
    {
        $response = array();
        $is_active = 1;
        try
        {
            $sql = "SELECT client_master_id, company_name FROM client_master_detail where client_master_id=?";
            $sql1 = "SELECT cl.client_master_id ,cd.company_name
                        FROM master_groups_company_list cl
                        LEFT JOIN client_master_detail cd ON cd.client_master_id=cl.client_master_id
                        WHERE cl.group_id = ? AND cl.client_master_id = ? AND cd.is_active= ?";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("i", $group_id);
                $stmt->execute();
                $stmt->bind_result($client_master_id, $company_name);
                $stmt->store_result();

                if ($stmt2 = $this->conn->prepare($sql1)) {
                    $stmt2->bind_param("iii", $group_id, $client_master_id, $is_active);
                    $stmt2->execute();
                    $stmt2->bind_result($client_master_id, $company_name);
                    $stmt2->store_result();
                    $pushArr = array();
                    if ($stmt->num_rows > 0 || $stmt2->num_rows > 0) {

                        while ($result = $stmt->fetch() || $result = $stmt2->fetch()) {
                            $sql_category = "SELECT  gc.account_category FROM group_companies_account_categories gc WHERE gc.group_id = ? and gc.client_master_id=? group by gc.account_category";

                            if ($stmte_category = $this->conn->prepare($sql_category)) {
                                $stmte_category->bind_param("ii", $group_id, $client_master_id);
                                $stmte_category->execute();
                                $stmte_category->bind_result($account_category);
                                $stmte_category->store_result();
                                $k = $stmte_category->num_rows;
                                $temp = array();
                                $category = array();
                                while ($compResult = $stmte_category->fetch()) {
                                    $account_category = $account_category;
                                    $sql2 = "SELECT  gc.account_type, gc.account_order, gc.account_type_alias
                                            FROM group_companies_account_categories gc
                                            WHERE gc.group_id = ? and account_category = ? and gc.client_master_id=?
                                            group by gc.account_type, gc.account_order,gc.account_type_alias
                                            ORDER BY gc.account_order ASC";
                                    if ($stmte = $this->conn->prepare($sql2)) {
                                        $stmte->bind_param("iii", $group_id, $account_category, $client_master_id);
                                        $stmte->execute();
                                        $stmte->bind_result($account_type, $account_order, $account_type_alias);
                                        $stmte->store_result();
                                        $k = $stmte->num_rows;

                                        $temp_data = array();
                                        while ($compResult = $stmte->fetch()) {
                                            $account_type = $account_type;
                                            $sql3 = "SELECT  gc.id, gc.account_code ,gc.account_name,gc.account_type,gc.account_category,gc.is_eliminated, gc.account_type_alias, gc.account_order
                                                     FROM group_companies_account_categories gc
                                                     WHERE gc.account_type = ? and gc.group_id = ? and gc.client_master_id =? and gc.account_name != 'Employee Compensation & Related Expenses' ORDER BY account_code ASC";

                                            if ($stmtes = $this->conn->prepare($sql3)) {
                                                $stmtes->bind_param("sii", $account_type, $group_id, $client_master_id);
                                                $stmtes->execute();
                                                $stmtes->bind_result($id, $account_code, $account_name, $account_type, $account_category, $is_eliminated, $account_type_alias, $account_order);
                                                $stmtes->store_result();
                                                $k = $stmtes->num_rows;

                                                $temp = array();
                                                while ($compResult = $stmtes->fetch()) {
                                                    $temp[] = array('category_id' => $id, 'account_code' => "$account_code",
                                                        'account_name' => "$account_name", 'account_type' => "$account_type",
                                                        'account_category' => $account_category, 'is_eliminated' => $is_eliminated,
                                                        "account_type_alias" => $account_type_alias, "account_order" => $account_order);
                                                }

                                            }
                                            $temp_data[] = array('account_type' => $account_type, 'account_type_alias' => $account_type_alias, 'account_order' => $account_order, "account_data" => $temp);
                                        }

                                    }
                                    $category[] = array('account_category' => $account_category, "category" => $temp_data);
                                }
                            }
                        }
                        $pushArr[] = array('group_name' => $group_name, 'group_id' => $group_id, 'company_name' => $company_name,
                            'client_master_id' => $client_master_id, "category_type_data" => $category);
                        $response["error"] = false;
                        $response['result'] = $pushArr;
                    } else {
                        $response["error"] = true;
                        $response['result'] = NO_COMPANY_FOUND;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateEliminate($group_company_id, $is_eliminated_key)
    {
        $response = array();
        try
        {
            $sql = "UPDATE group_companies_account_categories SET is_eliminated = ? WHERE id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $is_eliminated_key, $group_company_id);
                $result = $stmt->execute();
                $response["error"] = false;
                $response["message"] = Eliminate_UPDATE_SUCCESS;
            } else {
                $response["error"] = false;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function GetCompanyList($group_id)
    {
        $response = array();
        $is_active = 1;
        $tempEdit = array();
        $temp = array();
        try
        {
            if ($group_id == "") {
                $sql = "SELECT client_master_id,company_name FROM client_master_detail WHERE is_active = ? ";
            } else {
                $sql = "SELECT client_master_id,company_name FROM client_master_detail WHERE is_active = ? AND client_master_id NOT IN(SELECT client_master_id FROM master_groups_company_list WHERE group_id = ? )";
            }
            if ($stmt = $this->conn->prepare($sql)) {
                if ($group_id == "") {
                    $stmt->bind_param("i", $is_active);
                } else {
                    $stmt->bind_param("ii", $is_active, $group_id);
                }
                $stmt->execute();
                $stmt->bind_result($client_master_id, $company_name);
                $stmt->store_result();
                $response["error"] = fasle;
                while ($companyNameResult = $stmt->fetch()) {
                    $temp[] = array('client_master_id' => "$client_master_id", "company_name" => "$company_name");
                }
                if ($group_id == "") {
                    $response["right"] = array();
                } else {
                    $sqlleft = "SELECT client_master_id,company_name FROM client_master_detail WHERE is_active = ? AND
					client_master_id IN(SELECT client_master_id FROM master_groups_company_list WHERE group_id = ? )";
                    if ($stmtl = $this->conn->prepare($sqlleft)) {
                        $stmtl->bind_param("ii", $is_active, $group_id);
                        $stmtl->execute();
                        $stmtl->bind_result($client_master_id, $company_name);
                        $stmtl->store_result();
                        while ($companyResult = $stmtl->fetch()) {
                            $tempEdit[] = array('client_master_id' => "$client_master_id", "company_name" => "$company_name");
                        }
                        $response["right"] = $tempEdit;
                    }
                }
                $response["left"] = $temp;
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getGroupDetailList()
    {
        try {
            $sql = "SELECT GROUP_CONCAT(mcl.client_master_id) as client_master_id, cgm.group_id, cgm.client_id, cmd.company_name as group_name, cmd.default_currency, cmd.report_start_date FROM
                        client_master_detail as  cmd
                        JOIN client_group_mapping as cgm on cgm.group_id = cmd.client_master_id
                        JOIN master_groups_company_list as mcl on mcl.group_id = cgm.group_id
                        where cmd.is_group = 1
                        GROUP by  cgm.group_id";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->bind_result($client_master_id, $group_id, $client_id, $group_name, $default_currency, $report_start_date);
                $stmt->store_result();
                $pushArr = array();
                $group = array();
                if ($stmt->num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        $temp['group_id'] = $group_id;
                        $temp['client_id'] = $client_id;
                        $temp['group_name'] = $group_name;
                        $temp['default_currency'] = $default_currency;
                        $temp['report_start_date'] = $report_start_date;
                        $master_id_array = explode(',', $client_master_id);
                        $temp['group_details'] = $master_id_array;
                        $group[] = $temp;
                    }
                    $response["error"] = false;
                    $response['result'] = $group;
                } else {
                    $response["error"] = true;
                    $response['result'] = NO_COMPANY_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function GenerateGroupMetrics($group_id, $report_start_date, $sub_category_id1, $sub_category_id2)
    {
        try {

            $metricResponse = array();
            $utility = new utility();
            $dashboardReport = new dashboardReport();

            $start_month = date('m', $report_start_date);
            $now = new DateTime('now');
            $end_month = $now->format('m');
            $last_month = $end_month - 1;
            $mon_diff = $last_month - $start_month;
            if ($mon_diff == 1) {
                $days = 30;
            } else if ($mon_diff == 2) {
                $days = 60;
            } else {
                $days = 90;
            }

            $metric_path = __DIR__ . "/metrics/metric_" . $group_id . "_" . $report_start_date . ".json";

            $flash_report_file = $dashboardReport->MetricscheckCacheFileExist($group_id, 'flashreport', $sub_category_id1, $sub_category_id2);
            $flash_json_file = file_get_contents($flash_report_file);

            $flash_obj = json_decode($flash_json_file, true);

            $metricResponse["BankTransactions"] = null;
            $metricResponse["accountsSummarytotal"] = 0;
            $metricResponse["agedPayablestotal"] = 0;
            $metricResponse["agedReceivablestotal"] = 0;
            $metricResponse["estimatedPayrolltotal"] = 0;

            if (file_exists($flash_report_file)) {
                $metricResponse["BankTransactions"] = $flash_obj[7]["BankTransactions"];

                foreach ($flash_obj[1]['accountsSummary'] as $record) {
                    if ($record['bank_account_name'] == 'Total') {
                        $metricResponse["accountsSummarytotal"] = $record['closing_balance'];
                    }
                }

                foreach ($flash_obj[2]['agedPayables'] as $record) {
                    $metricResponse["agedPayablestotal"] = $record['total_payables']['Total'];
                }

                foreach ($flash_obj[3]['agedReceivables'] as $record) {
                    $metricResponse["agedReceivablestotal"] = $record['total_receivables']['Total'];
                }

                foreach ($flash_obj[4]['estimatedPayroll'][0] as $record) {
                    if ($record[0]['Value'] == 'Total Employee Compensation & Related Expenses') {
                        foreach ($record as $subrecord) {
                            if (is_numeric($subrecord['Value'])) {
                                $metricResponse["estimatedPayrolltotal"] = $subrecord['Value'];
                            }
                        }
                    }
                }
            }

            $pnl_report_file = $utility->MetricscheckCacheFileExist($group_id, 'plreport', $sub_category_id1, $sub_category_id2);
            $pnl_json_file = file_get_contents($pnl_report_file);
            $pnl_obj = json_decode($pnl_json_file, true);

            if (file_exists($pnl_report_file)) {

                //Account Receiveable
                $metricResponse["Account Receivables"] = $pnl_obj[1]['balanceSheet']['Current Assets']['Accounts Receivable'];

                //Account Payable
                $metricResponse["Account Payables"] = $pnl_obj[1]['balanceSheet']['Current Liabilities']['Accounts Payable'];

                //Total Income
                $metricResponse["Total Income"] = $pnl_obj[3]['trendPNL']['Income']['summaryRow']['Total Income'];

                $income_total_ebit = 0;
                foreach ($metricResponse["Total Income"] as $key => $val) {
                    $income_total_ebit = $income_total_ebit + $val["amount"];
                }

                //Operating Profit
                $metricResponse["Operating Profit"] = $pnl_obj[3]['trendPNL']['Operating Profit'];
                $operating_profit_total_ebit = 0;
                foreach ($metricResponse["Operating Profit"] as $key => $val) {
                    $operating_profit_total_ebit = $operating_profit_total_ebit + $val["amount"];
                }

                // EBIT Calculation
                $ebit = number_format($operating_profit_total_ebit / $income_total_ebit, 2);
                $metricResponse["EBIT"] = is_numeric($ebit) ? $ebit : 0;

                //Total Cost of Sales
                $metricResponse["Total Cost of Sales"] = $pnl_obj[3]['trendPNL']['Less Cost of Sales']['summaryRow']['Total Cost of Sales'];

                //Total Operating Expenses
                $metricResponse["Total Operating Expenses"] = $pnl_obj[3]['trendPNL']['Less Operating Expenses']['summaryRow']['Total Operating Expenses'];

                //Total Employee Compensation & Related Expenses
                $metricResponse["Total Employee Compensation"] = $pnl_obj[3]['trendPNL']['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];

                //Net Profit
                $metricResponse["Net Profit"] = $pnl_obj[3]['trendPNL']['Net Profit'];

                //Top 10 Operating Expenses
                $metricResponse["Operating Expenses"][] = array_slice($pnl_obj[3]['trendPNL']['Operating Expenses'], 0, TOP_N_RECORDS);

                //Total Other Income (Expenses)
                // $metricResponse["Total Other Income (Expenses)"] = $pnl_obj[3]['trendPNL']['Other Income (Expenses)']['summaryRow']['Total Other Income (Expenses)'];

                $data = array();
                $data = $dashboardReport->getEarlierMonthNumber($report_start_date);
                $dataLength = sizeof($data);

                // Account Receivable DSO calculation
                if (sizeof($metricResponse["Account Receivables"]) > 0) {
                    $account_receivable = array();
                    $total_income = array();
                    foreach ($metricResponse["Account Receivables"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $account_receivable[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Income"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_income[$array_id] = $val['amount'];
                        }
                    }

                    $account_receivable_total = 0;
                    $income_total = 0;

                    if ($dataLength == 1) {
                        $key = (int) $data[0];
                        $account_receivable_total = $account_receivable[$key];
                        $a = $account_receivable_total;
                        $income_total = $total_income[$key];
                        $b = $income_total / 30;
                        $c = $a / $b;
                    } elseif ($dataLength == 2) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key1];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1];
                        $b = $income_total / 60;
                        $c = $a / $b;
                    } elseif ($dataLength == 3) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key1];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1] + $total_income[$key2];
                        $b = $income_total / 90;
                        $c = $a / $b;
                    } elseif ($dataLength >= 4) {

                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $key3 = (int) $data[3];
                        $account_receivable_total = $account_receivable[$key] + $account_receivable[$key3];
                        $a = $account_receivable_total / 2;
                        $income_total = $total_income[$key] + $total_income[$key1] + $total_income[$key2];
                        $b = $income_total / 90;
                        $c = $a / $b;
                    }

                    $metricResponse["Accounts Receivable DSO"] = round(is_infinite($c) ? 0 : (is_nan($c) ? 0 : $c));
                } else {
                    $metricResponse["Accounts Receivable DSO"] = 0;
                }

                // Days Payable Outstanding (DPO) calculation
                if (sizeof($metricResponse["Account Payables"]) > 0) {
                    $account_payable_total = 0;
                    $cost_of_sales_total = 0;
                    $operating_expenses_total = 0;
                    $operating_profit_total = 0;
                    $employee_compensation_total = 0;

                    $account_payables = array();
                    $total_cost_of_sales = array();
                    $total_operating_expenses = array();
                    $total_employee_compensation = array();
                    foreach ($metricResponse["Account Payables"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $account_payables[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Cost of Sales"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_cost_of_sales[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Operating Expenses"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_operating_expenses[$array_id] = $val['amount'];
                        }
                    }

                    foreach ($metricResponse["Total Employee Compensation"] as $key => $val) {
                        if ($key != 0) {
                            $array_id = $val['month'];
                            $total_employee_compensation[$array_id] = $val['amount'];
                        }
                    }

                    if ($dataLength == 1) {
                        $key = (int) $data[0];
                        $account_payable_total = $account_payables[$key];
                        $d = $account_payable_total;
                        $cost_of_sales_total = $total_cost_of_sales[$key];
                        $operating_expenses_total = $total_operating_expenses[$key];
                        $employee_compensation_total = $total_employee_compensation[$key];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 30;
                        $g = $d / $f;
                    } elseif ($dataLength == 2) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key1];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 60;
                        $g = $d / $f;
                    } elseif ($dataLength == 3) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key1];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1] + $total_cost_of_sales[$key2];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1] + $total_operating_expenses[$key2];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1] + $total_employee_compensation[$key2];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 90;
                        $g = $d / $f;
                    } elseif ($dataLength >= 4) {
                        $key = (int) $data[0];
                        $key1 = (int) $data[1];
                        $key2 = (int) $data[2];
                        $key3 = (int) $data[3];
                        $account_payable_total = $account_payables[$key] + $account_payables[$key3];
                        $d = $account_payable_total / 2;
                        $cost_of_sales_total = $total_cost_of_sales[$key] + $total_cost_of_sales[$key1] + $total_cost_of_sales[$key2];
                        $operating_expenses_total = $total_operating_expenses[$key] + $total_operating_expenses[$key1] + $total_operating_expenses[$key2];
                        $employee_compensation_total = $total_employee_compensation[$key] + $total_employee_compensation[$key1] + $total_employee_compensation[$key2];
                        $e = $cost_of_sales_total + $operating_expenses_total - $employee_compensation_total;
                        $f = $e / 90;
                        $g = $d / $f;
                    }

                    $metricResponse["Days Payable Outstanding (DPO)"] = round(is_infinite($g) ? 0 : (is_nan($g) ? 0 : $g));
                } else {
                    $metricResponse["Days Payable Outstanding (DPO)"] = 0;
                }

                // Cash conversion cycle calculation
                $metricResponse["Cash Conversion Cycle"] = $metricResponse["Days Payable Outstanding (DPO)"] - $metricResponse["Accounts Receivable DSO"];

                $metricResponse["employees"] = $jfo["employees"];

                /* Current Ratio Calculation (Current Ratio = Current Assets / Current Liabilities) */
                // $metricResponse["Total Current Assets Last"] = end($pnl_obj[1]['balanceSheet']['Current Assets']['summaryRow']['Total Current Assets']);
                // $metricResponse["Total Bank Last"] = end($pnl_obj[1]['balanceSheet']['Bank']['summaryRow']['Total Bank']);
                // $metricResponse["Total Current Liabilities Last"] = end($pnl_obj[1]['balanceSheet']['Current Liabilities']['summaryRow']['Total Current Liabilities']);

                $metricResponse["Total Current Assets Last"] = end($dashboardReport->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Current Assets'));
                $metricResponse["Total Bank Last"] = end($dashboardReport->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Bank'));
                $metricResponse["Total Current Liabilities Last"] = end($dashboardReport->array_find_deep_by_key($pnl_obj[1]['balanceSheet'], 'Total Current Liabilities'));
                $metricResponse["last_updated_time"] = date('Y-m-d H:i:s');

            } else {
                $metricResponse["error"] = true;
                $metricResponse["message"] = NO_RECORD_FOUND;
            }

            file_put_contents($metric_path, json_encode($metricResponse));
            chmod($metric_path, 0777);
            //  $dashboardReport->updateCacheLog($group_id, true);
            return $metricResponse;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function deleteGroup($group_id)
    {

        try {
            $sql = "SELECT cgm.client_id  FROM client_group_mapping as cgm WHERE cgm.group_id = ?";
            if ($stmte = $this->conn->prepare($sql)) {
                $stmte->bind_param("i", $group_id);
                $stmte->execute();
                $stmte->bind_result($client_id);
                $stmte->store_result();
                if ($stmte->num_rows == 0) {
                    $sql1 = 'UPDATE `client_master_detail` SET is_group_deleted = ? WHERE `client_master_id` = ? and is_group = ?';
                    if ($stmt = $this->conn->prepare($sql1)) {
                        $is_group = 1;
                        $is_group_delete = 1;
                        $stmt->bind_param('iii', $is_group_delete, $group_id, $is_group);
                        $result = $stmt->execute();
                        $stmt->close();
                        if ($result) {
                            $this->conn->commit();
                            $response['error'] = false;
                            $response['message'] = GROUP_DELETE;
                        } else {
                            $response['error'] = true;
                        }
                    } else {
                        $response['error'] = true;
                        $response['message'] = GROUP_DELETE_FAILED;
                    }
                } else {
                    $response['error'] = true;
                    $response['message'] = GROUP_DELETE_FAILED_MAPPED;
                }
            } else {
                $response['error'] = true;
                $response['message'] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getJsonDataByGroup($client_id, $pdf_type, $sub_category_id1 = false, $sub_category_id2 = false, $client_master_id)
    {

        try {
            $report_obj = array();
            foreach ($client_master_id['group_details'] as $values) {
                $result = $this->getClientGroupDetails('', $client_id, "pl_report", $sub_category_id1, $sub_category_id2, $values);
                if ($result['error'] == false) {
                    $report_obj[] = $result['report_path'];
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            }
            return $report_obj;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getCompanyNameDataByGroup($client_id, $pdf_type, $sub_category_id1 = false, $sub_category_id2 = false, $client_master_id)
    {

        try {
            $report_obj = array();
            foreach ($client_master_id['group_details'] as $values) {
                $result = $this->getClientGroupDetails('', $client_id, "pl_report", $sub_category_id1, $sub_category_id2, $values);
                if ($result['error'] == false) {
                    $report_obj[] = $result['company_name'];
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            }
            return $report_obj;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getClientGroupDetails($group_id, $client_id, $pdf_type, $sub_category_id1 = false, $sub_category_id2 = false, $client_master_id)
    {
        try {

            $response = array();
            $sql = "SELECT cd.company_name, cm.name, cm.email, cd.client_master_id, cm.client_id, cd.report_start_date, cd.cushion_value
					FROM client_master_detail cd
					INNER JOIN client_user_mapping cum ON cum.client_master_id = cd.client_master_id
                    INNER JOIN client_master cm ON cm.client_id = cum.client_id
                    WHERE cum.client_master_id =  $client_master_id  and cm.is_active  = 1 and cum.client_id =$client_id ";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($company_name, $name, $email, $client_master_id, $client_id, $report_start_date, $cushion_value);
                    while ($stmt->fetch()) {
                        $metric_path = __DIR__ . "/metrics/metric_" . $client_master_id . "_" . $report_start_date . ".json";
                        $suffix = "";
                        if ($pdf_type == "flash_report") {
                            $report_dir_path = __DIR__ . "/flashreport/" . $client_master_id . "/";
                            $prefix = $client_master_id . "_flashreport_";
                        } else if ($pdf_type == "pl_report") {
                            $report_dir_path = __DIR__ . "/plreport/" . $client_master_id . "/";
                            $prefix = $client_master_id . "_plreport_";

                        }

                        if (!empty($sub_category_id1) || !empty($sub_category_id2)) {
                            $suffix = "";
                            $suffix .= $sub_category_id1;
                            if (strlen(trim($sub_category_id2)) > 0) {
                                $suffix .= "_" . $sub_category_id2;
                            }
                            $suffix .= ".json";
                        }

                        $latest_filename = $this->latestFileAtPath($report_dir_path, $prefix, $suffix);
                        $report_path = $report_dir_path . $latest_filename;

                        if (file_exists($metric_path) || file_exists($report_path)) {
                            $response["company_name"] = $company_name;
                            $response["name"] = $name;
                            $response["email"] = $email;
                            $response["client_master_id"] = $client_master_id;
                            $response["group_id"] = $group_id;
                            $response["client_id"] = $client_id;
                            //  $response["every_week_on"] = $every_week_on;
                            $response['report_start_date'] = $report_start_date;
                            $response['metrics_path'] = $metric_path;
                            $response["report_path"] = $report_path;
                            $response['cushion_value'] = $cushion_value;
                            $response["error"] = false;
                        }

                    }

                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getcurrencyBygroupID($group_id)
    {
        $response = array();
        $sql = "SELECT cd.default_currency, cd.client_master_id
                    FROM client_master_detail cd
                    WHERE cd.client_master_id =  $group_id ";

        if ($stmt = $this->conn->prepare($sql)) {
            $stmt->execute();
            $stmt->store_result();
            $stmt->num_rows;
            if ($stmt->num_rows > 0) {
                $stmt->bind_result($default_currency, $client_master_id);
                while ($stmt->fetch()) {
                    $response["default_currency"] = $default_currency;
                    $response["group_id"] = $client_master_id;
                }
            } else {
                $response["error"] = true;
                $response["message"] = NO_RECORD_FOUND;
            }
        } else {
            $response["error"] = true;
            $response["message"] = QUERY_EXCEPTION;
        }
        return $response;
    }

    public function getTrackingCategory($totalFiles, $filterBy)
    {
        $trackCatArray = "";
        $trackCatArray .= '"0":{';
        $trackCatArray .= '"error":false,';
        $trackCatArray .= '"message":"Record found",';
        $trackCatArray .= '"trackingCategories":';
        if ($filterBy == "SH") {
            $trackCatArray .= "{";
            $trackCatArray .= '"Category":[{';
            $trackCatArray .= '"sub_category_id": "1000",';
            $trackCatArray .= '"sub_category_name": "All"';
            $trackCatArray .= "}]";
            $trackCatArray .= "}";

        } else if ($filterBy == "group") {
            $trackCatArray .= json_encode($totalFiles);
        }
        $trackCatArray .= "},";
        return $trackCatArray;
    }

    public function trendpnlarray($trendPNL, $finalData)
    {
        /* income */
        $incomeRowData = $trendPNL->income();
        $incomeTotalData = $trendPNL->totalIncome($incomeRowData);
        if(!empty($incomeRowData)){
            $finalincomeData = array_merge($incomeRowData, $incomeTotalData);

            if ($finalincomeData == "") {
                $trend_pl_month_final_array['Income'] = array();
            } else {
                $trend_pl_month_final_array['Income'] = $finalincomeData;
            }
            ksort($trend_pl_month_final_array['Income']);
        }
        /* less cost of sales */
        $lessCostRowData = $trendPNL->LessCostofSales();
        $lesscostTotalData = $trendPNL->totalLessCostofSales($lessCostRowData);
        if(!empty($lessCostRowData)){
            $finallesscostData = array_merge($lessCostRowData, $lesscostTotalData);
            if ($finallesscostData == "") {
                $trend_pl_month_final_array['Less Cost of Sales'] = array();
            } else {
                $trend_pl_month_final_array['Less Cost of Sales'] = $finallesscostData;
            }
            ksort($trend_pl_month_final_array['Less Cost of Sales']);
        }
        /* gross profit */
        $grossProfit = $trendPNL->totalGrossProfit($incomeTotalData, $lesscostTotalData);
        if ($grossProfit == "") {
            $trend_pl_month_final_array['Gross Profit'] = array();
        } else {
            $trend_pl_month_final_array['Gross Profit'] = $grossProfit;
        }
        /* less operating expenses */
        $lessOPRowData = $trendPNL->LessOperatingExpenses();
        $empRowValue = $trendPNL->LessOperatingExpensesAndEmp();

        $employeeTotal = $empRowValue['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];
        unset($empRowValue['summaryRow']);
        $lesoperTotalData = $trendPNL->totalLessOper($lessOPRowData);
        $lesoperAndEmpTotalData = $trendPNL->totalLessOperAndEmp($lesoperTotalData, $employeeTotal);

        $finalLesOperData = array_merge($lessOPRowData, $empRowValue, $lesoperAndEmpTotalData);
        $finalEmpComp = $finalLesOperData['Employee Compensation & Related Expenses']['summaryRow'];

        unset($finalLesOperData['Employee Compensation & Related Expenses']['summaryRow']);
        if($finalEmpComp != ""){
            $finalLesOperData['Employee Compensation & Related Expenses']['summaryRow'] = $finalEmpComp;
            ksort($finalLesOperData['Employee Compensation & Related Expenses']);
        }

        if ($finalLesOperData == "") {
            $trend_pl_month_final_array['Less Operating Expenses'] = array();
        } else {
            $trend_pl_month_final_array['Less Operating Expenses'] = $finalLesOperData;
        }

        /* EBITDA */
        $finalEBITDA = $trendPNL->EBITA($grossProfit, $lesoperAndEmpTotalData);
        if ($finalEBITDA == "") {
            $trend_pl_month_final_array['Operating Profit'] = array();
        } else {
            $trend_pl_month_final_array['Operating Profit'] = $finalEBITDA;
        }
        /* non operating income */
        $nonOperIncomeRowData = $trendPNL->NonOperatingIncome();
        $nonOprincomeTotalData = $trendPNL->totalNonOperatingIncome($nonOperIncomeRowData);
        if(!empty($nonOperIncomeRowData)){
            $finalnonOprincomeData = array_merge($nonOperIncomeRowData, $nonOprincomeTotalData);
            if ($finalnonOprincomeData == "") {
                $trend_pl_month_final_array['Non-operating Income'] = array();
            } else {
                $trend_pl_month_final_array['Non-operating Income'] = $finalnonOprincomeData;
            }
        }
        /* non operating expenses */
        $nonOperexpRowData = $trendPNL->NonOperatingExpenses();
        $nonOprExpTotalData = $trendPNL->totalNonOperatingExp($nonOperexpRowData);
        if(!empty($nonOperexpRowData)){
            $finalnonOprExpData = array_merge($nonOperexpRowData, $nonOprExpTotalData);
            if ($finalnonOprExpData == "") {
                $trend_pl_month_final_array['Non-operating Expenses'] = array();
            } else {
                $trend_pl_month_final_array['Non-operating Expenses'] = $finalnonOprExpData;
            }
        }
        /* net profit */
        $finalNetProfit = $trendPNL->netProfit1($finalEBITDA, $nonOprincomeTotalData, $nonOprExpTotalData);
        if ($finalNetProfit == "") {
            $trend_pl_month_final_array['Net Profit'] = array();
        } else {
            $trend_pl_month_final_array['Net Profit'] = $finalNetProfit;
        }
        /* operating expenses */
        if ($finalData == "") {
            $trend_pl_month_final_array['Operating Expenses'] = array();
        } else {
            $trend_pl_month_final_array['Operating Expenses'] = $finalData;
        }
        return $trend_pl_month_final_array;
    }

    public function getEBIDA($trendPNL, $case)
    {
         /* income */
        $incomeRowData = $trendPNL->income();
        $incomeTotalData = $trendPNL->totalIncome($incomeRowData);
        if(!empty($incomeRowData)){
            $finalincomeData = array_merge($incomeRowData, $incomeTotalData);

            if ($finalincomeData == "") {
                $trend_pl_month_final_array['Income'] = array();
            } else {
                $trend_pl_month_final_array['Income'] = $finalincomeData;
            }
            ksort($trend_pl_month_final_array['Income']);
        }
        /* less cost of sales */
        $lessCostRowData = $trendPNL->LessCostofSales();
        $lesscostTotalData = $trendPNL->totalLessCostofSales($lessCostRowData);
        if(!empty($lessCostRowData)){
            $finallesscostData = array_merge($lessCostRowData, $lesscostTotalData);
            if ($finallesscostData == "") {
                $trend_pl_month_final_array['Less Cost of Sales'] = array();
            } else {
                $trend_pl_month_final_array['Less Cost of Sales'] = $finallesscostData;
            }
            ksort($trend_pl_month_final_array['Less Cost of Sales']);
        }
        /* gross profit */
        $grossProfit = $trendPNL->totalGrossProfit($incomeTotalData, $lesscostTotalData);
        if ($grossProfit == "") {
            $trend_pl_month_final_array['Gross Profit'] = array();
        } else {
            $trend_pl_month_final_array['Gross Profit'] = $grossProfit;
        }
        /* less operating expenses */
        $lessOPRowData = $trendPNL->LessOperatingExpenses();
        $empRowValue = $trendPNL->LessOperatingExpensesAndEmp();

        $employeeTotal = $empRowValue['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];
        unset($empRowValue['summaryRow']);
        $lesoperTotalData = $trendPNL->totalLessOper($lessOPRowData);
        $lesoperAndEmpTotalData = $trendPNL->totalLessOperAndEmp($lesoperTotalData, $employeeTotal);

        $finalLesOperData = array_merge($lessOPRowData, $empRowValue, $lesoperAndEmpTotalData);
        $finalEmpComp = $finalLesOperData['Employee Compensation & Related Expenses']['summaryRow'];

        unset($finalLesOperData['Employee Compensation & Related Expenses']['summaryRow']);
        if($finalEmpComp != ""){
            $finalLesOperData['Employee Compensation & Related Expenses']['summaryRow'] = $finalEmpComp;
            ksort($finalLesOperData['Employee Compensation & Related Expenses']);
        }

        if ($finalLesOperData == "") {
            $trend_pl_month_final_array['Less Operating Expenses'] = array();
        } else {
            $trend_pl_month_final_array['Less Operating Expenses'] = $finalLesOperData;
        }

        /* EBITDA */
        $finalEBITDA = $trendPNL->EBITA($grossProfit, $lesoperAndEmpTotalData);
        if ($finalEBITDA == "") {
            $trend_pl_month_final_array['Operating Profit'] = array();
        } else {
            $trend_pl_month_final_array['Operating Profit'] = $finalEBITDA;
        }
        /* non operating income */
        $nonOperIncomeRowData = $trendPNL->NonOperatingIncome();
        $nonOprincomeTotalData = $trendPNL->totalNonOperatingIncome($nonOperIncomeRowData);
        if(!empty($nonOperIncomeRowData)){
            $finalnonOprincomeData = array_merge($nonOperIncomeRowData, $nonOprincomeTotalData);
            if ($finalnonOprincomeData == "") {
                $trend_pl_month_final_array['Non-operating Income'] = array();
            } else {
                $trend_pl_month_final_array['Non-operating Income'] = $finalnonOprincomeData;
            }
        }
        /* non operating expenses */
        $nonOperexpRowData = $trendPNL->NonOperatingExpenses();
        $nonOprExpTotalData = $trendPNL->totalNonOperatingExp($nonOperexpRowData);
        if(!empty($nonOperexpRowData)){
            $finalnonOprExpData = array_merge($nonOperexpRowData, $nonOprExpTotalData);
            if ($finalnonOprExpData == "") {
                $trend_pl_month_final_array['Non-operating Expenses'] = array();
            } else {
                $trend_pl_month_final_array['Non-operating Expenses'] = $finalnonOprExpData;
            }
        }

        if ($case == "1") {
            return $finalEBITDA;
        } else if ($case == "2") {
            return $nonOprincomeTotalData;
        } else {
            return $nonOprExpTotalData;
        }
    }

    public function trendPNLCashFlow($trendPNL, $finalData)
    {
        /* income */
        $incomeRowData = $trendPNL->income();
        $incomeTotalData = $trendPNL->totalIncome($incomeRowData);
        $finalincomeData = array_merge($incomeRowData, $incomeTotalData);
        $trend_pl_month_final_array['Income'] = $finalincomeData;
        /* less cost of sales */
        $lessCostRowData = $trendPNL->LessCostofSales();
        $lesscostTotalData = $trendPNL->totalLessCostofSales($lessCostRowData);
        $finallesscostData = array_merge($lessCostRowData, $lesscostTotalData);
        $trend_pl_month_final_array['Less Cost of Sales'] = $finallesscostData;
        /* gross profit */
        $grossProfit = $trendPNL->totalGrossProfit($incomeTotalData, $lesscostTotalData);
        $trend_pl_month_final_array['Gross Profit'] = $grossProfit;
        /* less operating expenses */
        $lessOPRowData = $trendPNL->LessOperatingExpenses();
        $empRowValue = $trendPNL->LessOperatingExpensesAndEmp();

        $employeeTotal = $empRowValue['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];
        unset($empRowValue['summaryRow']);
        $lesoperTotalData = $trendPNL->totalLessOper($lessOPRowData);
        $lesoperAndEmpTotalData = $trendPNL->totalLessOperAndEmp($lesoperTotalData, $employeeTotal);
        $finalLesOperData = array_merge($lessOPRowData, $empRowValue, $lesoperAndEmpTotalData);
        $trend_pl_month_final_array['Less Operating Expenses'] = $finalLesOperData;
        /* EBITDA */
        $finalEBITDA = $trendPNL->EBITA($grossProfit, $lesoperAndEmpTotalData);
        $trend_pl_month_final_array['Operating Profit'] = $finalEBITDA;
        /* non operating income */
        $nonOperIncomeRowData = $trendPNL->NonOperatingIncome();
        $nonOprincomeTotalData = $trendPNL->totalNonOperatingIncome($nonOperIncomeRowData);
        $finalnonOprincomeData = array_merge($nonOperIncomeRowData, $nonOprincomeTotalData);
        $trend_pl_month_final_array['Non-operating Income'] = $finalnonOprincomeData;
        /* non operating expenses */
        $nonOperexpRowData = $trendPNL->NonOperatingExpenses();
        $nonOprExpTotalData = $trendPNL->totalNonOperatingExp($nonOperexpRowData);
        $finalnonOprExpData = array_merge($nonOperexpRowData, $nonOprExpTotalData);
        $trend_pl_month_final_array['Non-operating Expenses'] = $finalnonOprExpData;
        /* net profit */
        $finalNetProfit = $trendPNL->netProfit1($finalEBITDA, $nonOprincomeTotalData, $nonOprExpTotalData);
        $trend_pl_month_final_array['Net Profit'] = $finalNetProfit;
        /* operating expenses */
        $trend_pl_month_final_array['Operating Expenses'] = $finalData;
        $finalData['trendPNL'] = $trend_pl_month_final_array;

        return $finalData;
    }

    public function quarterlyArray($finalArray)
    {
        $dash = new dashboardReport();
        $quarters = $dash->buildQuartelyMonthArray();
        $finalArrayQuartely = array();
        $array_flag = 1;
        foreach ($finalArray as $k1 => $v1) {
            foreach ($v1 as $k2 => $v2) {
                    if (is_array($v2) && !array_key_exists('date', $v2)) {
                        foreach ($v2 as $k3 => $v3) {
                            if (is_array($v3) && !array_key_exists('date', $v3)) {
                                foreach ($v3 as $k4 => $v4) {
                                    if (is_array($v4) && !array_key_exists('date', $v4)) {
                                        $finalArrayQuartely[$k1][$k2][$k3][$k4] = $dash->buildQuartelyRecord($v4, $quarters);
                                    } else {
                                        $array_flag = 0;
                                        break;
                                    }
                                }
                                if ($array_flag == 0) {
                                    $finalArrayQuartely[$k1][$k2][$k3] = $dash->buildQuartelyRecord($v3, $quarters);
                                    $array_flag = 1;
                                }
                            } else {
                                $array_flag = 0;
                                break;
                            }
                        }
                        if ($array_flag == 0) {
                            $finalArrayQuartely[$k1][$k2] = $dash->buildQuartelyRecord($v2, $quarters);
                            $array_flag = 1;
                        }
                    } else {
                        $array_flag = 0;
                        break;
                    }
            }
            if ($array_flag == 0) {
                $finalArrayQuartely[$k1] = $dash->buildQuartelyRecord($v1, $quarters);
                $array_flag = 1;
            }
        }
        return $finalArrayQuartely;
    }

    public function balanceSheetArraycash($balanceSheet)
    {
        $bankRowData = $balanceSheet->Bank();
        $totalBankData = $balanceSheet->TotalBank($bankRowData);
        $finalBankData = array_merge($bankRowData, $totalBankData);
        $balancesheet_final_array['Bank'] = $finalBankData;

        $CurrentAssetsRowData = $balanceSheet->CurrentAssets();
        $totalCurrentAssetsData = $balanceSheet->TotalCurrentAssets($CurrentAssetsRowData);
        $finalCurrentAssetsData = array_merge($CurrentAssetsRowData, $totalCurrentAssetsData);
        $balancesheet_final_array['Current Assets'] = $finalCurrentAssetsData;

        $fixedAssetsrowData = $balanceSheet->FixedAssets();
        $totalFixedAssetsData = $balanceSheet->TotalFixedAssets($fixedAssetsrowData);
        $finalFixedAssetsData = array_merge($fixedAssetsrowData, $totalFixedAssetsData);
        $balancesheet_final_array['Fixed Assets'] = $finalFixedAssetsData;

        $nonCurrentAssetsRowData = $balanceSheet->NonCurrentAssets();
        $totalNonCurrentAssetsData = $balanceSheet->TotalNonCurrentAssets($nonCurrentAssetsRowData);
        $finalNonCurrentAssets = array_merge($nonCurrentAssetsRowData, $totalNonCurrentAssetsData);
        $balancesheet_final_array['Non-Current Assets'] = $finalNonCurrentAssets;

        $totalAssets = $balanceSheet->totalAssetsData($totalBankData, $totalCurrentAssetsData, $totalFixedAssetsData, $totalNonCurrentAssetsData);
        $balancesheet_final_array['Total Assets'] = $totalAssets;

        $liabitiesRowData = $balanceSheet->CurrentLiabilities();
        $totalLiabitiesData = $balanceSheet->totalCurrentLiabities($liabitiesRowData);
        $finalLiabitiesData = array_merge($liabitiesRowData, $totalLiabitiesData);
        $balancesheet_final_array['Current Liabilities'] = $finalLiabitiesData;

        if (!empty($balanceSheet->NonCurrentLiabilities())) {

            $nonCurrliabitiesRowData = $balanceSheet->NonCurrentLiabilities();
            $totalnonCurrLiabitiesData = $balanceSheet->totalNonCurrentLiabilities($nonCurrliabitiesRowData);
            $finaltotalnonCurrLiabitiesData = array_merge($nonCurrliabitiesRowData, $totalnonCurrLiabitiesData);
            $balancesheet_final_array['Non-Current Liabilities'] = $finaltotalnonCurrLiabitiesData;
        }

        $totlLiabilities = $balanceSheet->totalLiabilitiess($totalLiabitiesData, $totalnonCurrLiabitiesData);
        $balancesheet_final_array['Total Liabilities'] = $totlLiabilities;

        $totalAssets = $balanceSheet->totalnetAssets($totalAssets, $totlLiabilities);
        $balancesheet_final_array['Net Assets'] = $totalAssets;
        $equityRowData = $balanceSheet->Equity();
        unset($equityRowData['Adjustment Upon Consolidation']);
        $array3 = [];
        foreach ($equityRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                        $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                    }
                }
            }
        }

        $finalarray3 = collect($array3)->collapse()->toArray();
        $AdjustmentUponConsolidation = $finalarray3;

        $result = [];

        $collectTotalAssets = collect($totalAssets)->groupBy('id')->toArray();
        $collectAdjustmentUponConsolidation = collect($AdjustmentUponConsolidation)->groupBy('id')->toArray();
        unset($collectTotalAssets['no_expand']);
        foreach ($collectTotalAssets as $akey => $value) {
            if (is_array($value)) {
                if ($value[0]['id'] == $collectAdjustmentUponConsolidation[$akey][0]['id']) {
                    $value[0]['amount'] = $value[0]['amount'] - ($collectAdjustmentUponConsolidation[$akey][0]['amount']);
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        $equityRowData['Adjustment Upon Consolidation'] = $finalresult;

        $totalequityData = $balanceSheet->totalEquity($equityRowData);
        $finalequityData = array_merge($equityRowData, $totalequityData);
        $balancesheet_final_array['Equity'] = $finalequityData;

        $finalData['balanceSheet'] = $balancesheet_final_array;
        return $finalData;

    }

    public function ArrayUnset($Array, $Find)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $key2 => $value2) {
                        if ($key2 == $Find) {
                            unset($Array[$Key][$key2]);
                        }
                    }
                } else {
                    $Array[$Key] = $this->ArrayUnset($Array[$Key], $Find);
                }
            }
        }
        return $Array;
    }

    public function readJson($filePath, $toCur, $currency_value, $eliminate_path)
    {
        if ($filePath == $eliminate_path['path']) {
            $jsondata = json_decode(file_get_contents($filePath), true);
            foreach ($eliminate_path as $array_value) {
                if (is_array($array_value)) {
                    if ($array_value['account_category'] == 1) {
                        $jsondata[3]['trendPNL'] = $this->ArrayUnset($jsondata[3]['trendPNL'], $array_value['account_name'], "");
                    } else {
                        $jsondata[1]['balanceSheet'] = $this->ArrayUnset($jsondata[1]['balanceSheet'], $array_value['account_name'], "");
                    }
                }
            }
            $json = $jsondata;

        } else {
            $json = json_decode(file_get_contents($filePath), true);
        }

        if ($json['default_currency'] !== $toCur) {
            $currency = $currency_value;
            $jsondata = $json;
            if (isset($jsondata[3]['trendPNL']['trendPNL_Quartely'])) {
                $jsondata[3]['trendPNL']['trendPNL_Quartely'] = $this->trendPNLQuartely($jsondata[3]['trendPNL']['trendPNL_Quartely'], $currency);
            }
        } else {
            $currency = $toCur;
            $jsondata = $json;
        }
        return $jsondata;
    }

    public function trendPNLQuartely($Array, $Replace)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    $Array[$Key] = $this->trendPNLQuartely($Array[$Key], $Replace);
                } else {
                    if (is_numeric($Val)) {
                        $Array[$Key] = round($Array[$Key] * $Replace);
                    }
                }
            }
        }
        return $Array;
    }

    public function ArrayReplace($Array, $Find, $Replace)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if ($Key === 'Operating Expenses') {
                        foreach ($Val as $key2 => $value2) {

                            if (is_numeric($value2)) {

                                $Array[$Key][$key2] = floatval($Array[$Key][$key2]) * $Replace;
                            }
                        }
                    } else {
                        $Array[$Key] = $this->ArrayReplace($Array[$Key], $Find, $Replace);
                    }
                } else {
                    if ($Key == $Find) {
                        if (is_numeric($Array[$Key])) {

                            $Array[$Key] = round($Array[$Key] * $Replace);
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function ArraySearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {

                if (is_array($Array[$Key])) {
                    if (array_key_exists('id', $Array[$Key])) {

                        if (strtotime($Array[$Key]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key]);
                        } else {
                            $amount += $Array[$Key]['amount'];
                            $Array['total'] = $amount;
                        }
                    }
                    $Array[$Key] = $this->ArraySearch($Array[$Key], $Find, $startDate);
                }
            }
        }
        return $Array;
    }
    public function ArraySearchss($Array)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if (empty($Array[$Key])) {
                        $Array[$Key] = 0;
                    }
                    if (array_key_exists('total', $Array[$Key])) {
                        $Array[$Key] = $Array[$Key]['total'];
                    }
                    $Array[$Key] = $this->ArraySearchss($Array[$Key]);
                }
            }
        }
        return $Array;
    }

    public function ArrayFilter($Array)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    $Array[$Key] = $this->ArrayFilter($Array[$Key]);
                } else {
                    if (empty($Val)) {
                        unset($Array[$Key]);
                    }
                }
            }
        }
        return $Array;
    }

    public function ArrayRemove($Array, $Find)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if ($Key == $Find) {
                    unset($Array[$Key]);
                } else {
                    if (is_array($Array[$Key])) {
                        $Array[$Key] = $this->ArrayRemove($Array[$Key], $Find);
                    }

                }

            }
        }
        return $Array;
    }
    public function recursiveRemoval(&$array, $val)
    {
        if (is_array($array)) {
            foreach ($array as $key => &$arrayElement) {
                if (is_array($arrayElement)) {
                    $this->recursiveRemoval($arrayElement, $val);
                } else {
                    if ($arrayElement == $val) {
                        unset($array[$key]);
                    }
                }
            }
        }
    }

    public function array_merge_recursive_numeric()
    {
        // Gather all arrays
        $arrays = func_get_args();
        // If there's only one array, it's already merged
        if (count($arrays) == 1) {
            return $arrays[0];
        }
        // Remove any items in $arrays that are NOT arrays
        foreach ($arrays as $key => $array) {
            if (!is_array($array)) {
                unset($arrays[$key]);
            }
        }
        // We start by setting the first array as our final array.
        // We will merge all other arrays with this one.
        $final = array_shift($arrays);
        foreach ($arrays as $b) {
            foreach ($final as $key => $value) {
                // If $key does not exist in $b, then it is unique and can be safely merged
                if (!isset($b[$key])) {
                    $final[$key] = $value;
                } else {
                    // If $key is present in $b, then we need to merge and sum numeric values in both
                    if ($key !== 'year' && $key !== 'month' && is_numeric($value) && is_numeric($b[$key])) {
                        // If both values for these keys are numeric, we sum them
                        $final[$key] = $value + $b[$key];
                    } else if (is_array($value) && is_array($b[$key])) {
                        // If both values are arrays, we recursively call ourself
                        $final[$key] = $this->array_merge_recursive_numeric($value, $b[$key]);
                    } else {
                        // If both keys exist but differ in type, then we cannot merge them.
                        // In this scenario, we will $b's value for $key is used
                        $final[$key] = $b[$key];
                    }
                }
            }
            // Finally, we need to merge any keys that exist only in $b
            foreach ($b as $key => $value) {
                if (!isset($final[$key])) {
                    $final[$key] = $value;
                }
            }
        }
        return $final;
    }

    public function array_merge_recursive_amount()
    {
        // Gather all arrays
        $arrays = func_get_args();

        // If there's only one array, it's already merged
        if (count($arrays) == 1) {
            return $arrays[0];
        }
        // Remove any items in $arrays that are NOT arrays
        foreach ($arrays as $key => $array) {
            if (!is_array($array)) {
                unset($arrays[$key]);
            }
        }
        // We start by setting the first array as our final array.
        // We will merge all other arrays with this one.
        $final = array_shift($arrays);

        foreach ($arrays as $b) {
            foreach ($final as $key => $value) {
                // If $key does not exist in $b, then it is unique and can be safely merged
                if (!isset($b[$key])) {
                    $final[$key] = $value;
                } else {
                    // If $key is present in $b, then we need to merge and sum numeric values in both
                    if (is_numeric($value) && is_numeric($b[$key] && $key === "amount")) {
                        // If both values for these keys are numeric, we sum them
                        $final[$key] = $value + $b[$key];
                    } else if (is_array($value) && is_array($b[$key])) {
                        // If both values are arrays, we recursively call ourself
                        $final[$key] = $this->array_merge_recursive_amount($value, $b[$key]);
                    } else {
                        // If both keys exist but differ in type, then we cannot merge them.
                        // In this scenario, we will $b's value for $key is used
                        $final[$key] = $b[$key];
                    }
                }
            }
            // Finally, we need to merge any keys that exist only in $b
            foreach ($b as $key => $value) {
                if (!isset($final[$key])) {
                    $final[$key] = $value;
                }
            }
        }
        return $final;
    }

    //get all group report values

    public function getTrendFinalReport($trendPNL, $finalData)
    {

        $trendpnlarray = "";
        $trendpnlarray .= '"3":{';
        $trendpnlarray .= '"error":false,';
        $trendpnlarray .= '"message":"Record found",';
        $trendpnlarray .= '"trendPNL": ';
        $trendpnlMonthly_report = $this->trendpnlarray($trendPNL, $finalData);
        $trendpnlMonthly_report["trendPNL_Quartely"] = $this->quarterlyArray($trendpnlMonthly_report);
        $trendpnlarray .= json_encode($trendpnlMonthly_report);
        $trendpnlarray .= '},';
        return $trendpnlarray;
    }

    public function consolidateMTDFinalReport($MTDValues)
    {
        $jsondata = '"2":{';
        $jsondata .= '"error":false,';
        $jsondata .= '"message":"Record found",';
        $jsondata .= '"consolidatedPNL_mtd": {';
        $jsondata .= '"consolidatedPNL_mtd": ';
        $jsondata .= json_encode($this->utf8ize($MTDValues));
        $jsondata .= '}';
        $jsondata .= ',';
        return $jsondata;
    }

    public function consolidateYTDFinalReport($YTDValues)
    {
        $jsondata = '"consolidatedPNL_ytd": {';
        $jsondata .= '"consolidatedPNL_ytd": ';
        $jsondata .= json_encode($this->utf8ize($YTDValues));
        $jsondata .= '}';
        $jsondata .= ',';
        return $jsondata;
    }

    public function consolidateYTYFinalReport($YTYValues)
    {
        $jsondata = '"consolidatedPNL_yty": {';
        $jsondata .= '"consolidatedPNL_yty": ';
        $jsondata .= json_encode($this->utf8ize($YTYValues));
        $jsondata .= '},';
        return $jsondata;
    }

    public function consolidateYTDMTDFinalReport($consolidatePNLYTYandYTD)
    {
        $jsondata = '"consolidatedPNL_ytd_mtd": {';
        $jsondata .= '"consolidatedPNL_ytd_mtd": ';
        $jsondata .= json_encode($this->utf8ize($consolidatePNLYTYandYTD));
        $jsondata .= '}';
        $jsondata .= '},';
        return $jsondata;
    }

    public function balanceSheetArray($balanceSheet)
    {
        $bankRowData = $balanceSheet->Bank();
        $totalBankData = $balanceSheet->TotalBank($bankRowData);
        $finalBankData = array_merge($bankRowData, $totalBankData);
        $balancesheet_final_array['Bank'] = $finalBankData;

        $CurrentAssetsRowData = $balanceSheet->CurrentAssets();
        $totalCurrentAssetsData = $balanceSheet->TotalCurrentAssets($CurrentAssetsRowData);
        $finalCurrentAssetsData = array_merge($CurrentAssetsRowData, $totalCurrentAssetsData);
        $balancesheet_final_array['Current Assets'] = $finalCurrentAssetsData;

        $fixedAssetsrowData = $balanceSheet->FixedAssets();
        $totalFixedAssetsData = $balanceSheet->TotalFixedAssets($fixedAssetsrowData);
        $finalFixedAssetsData = array_merge($fixedAssetsrowData, $totalFixedAssetsData);
        $balancesheet_final_array['Fixed Assets'] = $finalFixedAssetsData;

        $nonCurrentAssetsRowData = $balanceSheet->NonCurrentAssets();
        $totalNonCurrentAssetsData = $balanceSheet->TotalNonCurrentAssets($nonCurrentAssetsRowData);
        $finalNonCurrentAssets = array_merge($nonCurrentAssetsRowData, $totalNonCurrentAssetsData);
        $balancesheet_final_array['Non-Current Assets'] = $finalNonCurrentAssets;

        $totalAssets = $balanceSheet->totalAssetsData($totalBankData, $totalCurrentAssetsData, $totalFixedAssetsData, $totalNonCurrentAssetsData);
        $balancesheet_final_array['Total Assets'] = $totalAssets;

        $liabitiesRowData = $balanceSheet->CurrentLiabilities();
        $totalLiabitiesData = $balanceSheet->totalCurrentLiabities($liabitiesRowData);
        $finalLiabitiesData = array_merge($liabitiesRowData, $totalLiabitiesData);
        $balancesheet_final_array['Current Liabilities'] = $finalLiabitiesData;

        if (!empty($balanceSheet->NonCurrentLiabilities())) {

            $nonCurrliabitiesRowData = $balanceSheet->NonCurrentLiabilities();
            $totalnonCurrLiabitiesData = $balanceSheet->totalNonCurrentLiabilities($nonCurrliabitiesRowData);
            $finaltotalnonCurrLiabitiesData = array_merge($nonCurrliabitiesRowData, $totalnonCurrLiabitiesData);
            $balancesheet_final_array['Non-Current Liabilities'] = $finaltotalnonCurrLiabitiesData;
        }

        $totlLiabilities = $balanceSheet->totalLiabilitiess($totalLiabitiesData, $totalnonCurrLiabitiesData);
        $balancesheet_final_array['Total Liabilities'] = $totlLiabilities;

        $totalAssets = $balanceSheet->totalnetAssets($totalAssets, $totlLiabilities);
        $balancesheet_final_array['Net Assets'] = $totalAssets;
        $equityRowData = $balanceSheet->Equity();
        
        unset($equityRowData['Adjustment Upon Consolidation']);
        $array3 = [];
        foreach ($equityRowData as $first_array) {
            $groupArray = collect($first_array)->groupBy('id')->toArray();
            if (empty($array3)) {
                $array3 = $groupArray;
            } else {
                foreach ($groupArray as $arrkey => $arrvalue) {
                    if ($array3[$arrkey][0]['id'] == $arrvalue[0]['id']) {
                        $array3[$arrkey][0]['amount'] += $arrvalue[0]['amount'];
                    }
                }
            }
        }

        $finalarray3 = collect($array3)->collapse()->toArray();
        $AdjustmentUponConsolidation = $finalarray3;

        $result = [];
        $collectTotalAssets = collect($totalAssets)->groupBy('id')->toArray();

        $collectAdjustmentUponConsolidation = collect($AdjustmentUponConsolidation)->groupBy('id')->toArray();
        unset($collectTotalAssets['no_expand']);
        foreach ($collectTotalAssets as $akey => $value) {
            if (is_array($value)) {
                if ($value[0]['id'] == $collectAdjustmentUponConsolidation[$akey][0]['id']) {
                    $value[0]['amount'] = $value[0]['amount'] - ($collectAdjustmentUponConsolidation[$akey][0]['amount']);
                }
            }

            $result[] = $value;
        }
        $finalresult = collect($result)->collapse()->toArray();
        $equityRowData['Adjustment Upon Consolidation'] = $finalresult;

        $totalequityData = $balanceSheet->totalEquity($equityRowData);
        $finalequityData = array_merge($equityRowData, $totalequityData);
        $balancesheet_final_array['Equity'] = $finalequityData;

        return $balancesheet_final_array;

    }

    public function getBalancesheetReportValues($files, $balanceSheet)
    {
        $balanceSheetArray .= "";
        $balanceSheetArray .= '"1":{';
        $balanceSheetArray .= '"error":false,';
        $balanceSheetArray .= '"message":"Record found",';
        $balanceSheetArray .= '"balanceSheet": ';
        $balancesheetFinalArray = $this->balanceSheetArray($balanceSheet);
        $balanceSheetArray .= json_encode($this->utf8ize($balancesheetFinalArray));
        $balanceSheetArray .= '},';
        return $balanceSheetArray;
    }

    public function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_object($d)) {
            foreach ($d as $k => $v) {
                $d->$k = $this->utf8ize($v);
            }
        } else {
            return utf8_encode($d);
        }

        return $d;
    }

    public function getCurrencyList()
    {
        $currencyListArray = "";
        $currencyListArray .= '"4":{';
        $currencyListArray .= '"error":false,';
        $currencyListArray .= '"message":"Record found",';
        $currencyListArray .= '"currencyList":[';
        $currencyListArray .= "]";
        $currencyListArray .= "},";
        return $currencyListArray;
    }

    public function cashflowFinalReport($final_jsonclow)
    {
        $cashFlowArray = "";
        $cashFlowArray .= '"5":{';
        $cashFlowArray .= '"error":false,';
        $cashFlowArray .= '"message":"Record found",';
        $cashFlowArray .= '"cashFlowStatement":';
        $cashFlowArray .= json_encode($this->utf8ize($final_jsonclow));
        $cashFlowArray .= '},';
        return $cashFlowArray;
    }

    public function getinvoiceByNameReport()
    {
        $invoiceArray = "";
        $invoiceArray .= '"6":{';
        $invoiceArray .= '"error":false,';
        $invoiceArray .= '"message":"Record found",';
        $invoiceArray .= '"invoiceByName":{';
        $invoiceArray .= '"":[';
        $invoiceArray .= "]";
        $invoiceArray .= "}";
        $invoiceArray .= "},";
        return $invoiceArray;
    }

    public function currencyConversion($files, $toCur, $currency_value)
    {
        $currencyconversionArray = "";
        $currencyconversionArray = '"7":{';
        $currencyconversionArray .= '"error":false,';
        $currencyconversionArray .= '"message":"Record found",';
        $currencyconversionArray .= '"currencyConversion":{';
        $currencyconversionArray .= '}';
        $currencyconversionArray .= '}';
        return $currencyconversionArray;
    }

    public function getFlashCurrByCurDate($files, $toCur)
    {
        $curDate = date('Y-m-d', strtotime('-1 day'));
        foreach ($files as $filePath) {
            $json = json_decode(file_get_contents($filePath), true);
            if ($json['default_currency'] !== $toCur) {
                $context = stream_context_create(array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
                    ),
                ));
                $data = file_get_contents(HISTORIC_CURRENCY_URL . '/?from=' . $toCur . '&to=' . $json['default_currency'] . '&date=' . $curDate . '&amount=1', false, $context);
                $data = json_decode($data, true);
                $currency[] = $data['to'][0]['mid'];
                $jsondata = $currency;
            } else {
                $currency[] = 1;
                $jsondata = $currency;
            }
        }
        return $jsondata;
    }

    public function ConsolidateArraySearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if ($Key == "Gross Profit" || $Key == "Operating Profit" || $Key == "Net Profit") {
                    $Array[$Key] = $this->conYTDandYTYProfitsearch($Array[$Key], $Find, $startDate);
                }
                if (is_array($Array[$Key])) {
                    $summaryData = $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'];
                    $empRowValue = $Array[$Key]['Employee Compensation & Related Expenses'];
                    $empdata = $this->conYTDandYTYEMPsearch($empRowValue, $Find, $startDate);
                    if (is_array($empdata)) {
                        $Array[$Key]['Employee Compensation & Related Expenses'] = $empdata;
                    }
                    $empsummary = $this->conYTDandYTYEMPsearch($summaryData, $Find, $startDate);
                    if (is_array($empsummary)) {
                        $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'] = $empsummary;
                    }
                    foreach ($Array[$Key] as $Key1 => $Val1) {
                        $amount = 0;
                        foreach ($Array[$Key][$Key1] as $Key2 => $Val2) {
                            if (!is_numeric($Key2)) {
                                if ($Key2 == "Total Income" || $Key2 == "Total Cost of Sales" || $key2 == "Total Operating Expenses" || $Key2 == "Total Non-operating Income" || $Key2 == "Total Non-operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conYTDandYTYsearch($Array[$Key][$Key1], $Find, $startDate);
                                } else if ($Key2 == "Total Operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conYTDandYTYsearch($Array[$Key][$Key1], $Find, $startDate);
                                }

                            } else {
                                if (strtotime($Array[$Key][$Key1][$Key2]['date']) <= strtotime($startDate)) {
                                    unset($Array[$Key][$Key1][$Key2]);
                                } else {
                                    $amount += $Array[$Key][$Key1][$Key2]['amount'];
                                    $Array[$Key][$Key1]['total'] = "$amount";
                                    unset($Array[$Key][$Key1][$Key2]);
                                }
                            }
                        }
                    }
                } else {
                    $Array[$Key] = $this->conYTDandYTYsearch($Array[$Key], $Find, $startDate);
                }
            }
        }
        unset($Array['Operating Expenses']);
        return $Array;
    }

    public function ConsEndDateArraySearch($Array, $Find, $startDate, $endDate)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                if ($Key == "Gross Profit" || $Key == "Operating Profit" || $Key == "Net Profit") {
                    $Array[$Key] = $this->conYTDandYTYProfitByEndDate($Array[$Key], $Find, $startDate, $endDate);
                }
                if (is_array($Array[$Key])) {
                    $summaryData = $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'];
                    $empRowValue = $Array[$Key]['Employee Compensation & Related Expenses'];
                    $empdata = $this->conYTDandYTYEMPByEndDate($empRowValue, $Find, $startDate, $endDate);
                    if (is_array($empdata)) {
                        $Array[$Key]['Employee Compensation & Related Expenses'] = $empdata;
                    }
                    $empsummary = $this->conYTDandYTYEMPByEndDate($summaryData, $Find, $startDate, $endDate);
                    if (is_array($empsummary)) {
                        $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'] = $empsummary;
                    }
                    foreach ($Array[$Key] as $Key1 => $Val1) {
                        $amount = 0;
                        foreach ($Array[$Key][$Key1] as $Key2 => $Val2) {
                            if (!is_numeric($Key2)) {
                                if ($Key2 == "Total Income" || $Key2 == "Total Cost of Sales" || $key2 == "Total Operating Expenses" || $Key2 == "Total Non-operating Income" || $Key2 == "Total Non-operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conYTDandYTYByEndDate($Array[$Key][$Key1], $Find, $startDate, $endDate);
                                } else if ($Key2 == "Total Operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conYTDandYTYByEndDate($Array[$Key][$Key1], $Find, $startDate, $endDate);
                                }

                            } else {
                                if (strtotime($Array[$Key][$Key1][$Key2]['date']) <= strtotime($startDate)) {
                                    unset($Array[$Key][$Key1][$Key2]);
                                } else {
                                    if (strtotime($Array[$Key][$Key1][$Key2]['date']) <= strtotime($endDate)) {
                                        $amount += $Array[$Key][$Key1][$Key2]['amount'];
                                        $Array[$Key][$Key1]['total'] = "$amount";
                                        unset($Array[$Key][$Key1][$Key2]);
                                    } else {
                                        unset($Array[$Key][$Key1][$Key2]);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $Array[$Key] = $this->conYTDandYTYByEndDate($Array[$Key], $Find, $startDate);
                }
            }
        }
        unset($Array['Operating Expenses']);
        return $Array;
    }

    public function ConsMTDArraySearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if ($Key == "Gross Profit" || $Key == "Operating Profit" || $Key == "Net Profit") {
                    $Array[$Key] = $this->conMTDProfitsearch($Array[$Key], $Find, $startDate);
                }
                $summaryData = $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'];
                $empRowValue = $Array[$Key]['Employee Compensation & Related Expenses'];
                $empdata = $this->conMTDEMPsearch($empRowValue, $Find, $startDate);
                if (is_array($empdata)) {
                    $Array[$Key]['Employee Compensation & Related Expenses'] = $empdata;
                }
                $empsummary = $this->conMTDEMPsearch($summaryData, $Find, $startDate);
                if (is_array($empsummary)) {
                    $Array[$Key]['Employee Compensation & Related Expenses']['summaryRow'] = $empsummary;
                }

                if (is_array($Array[$Key])) {
                    foreach ($Array[$Key] as $Key1 => $Val1) {
                        foreach ($Array[$Key][$Key1] as $Key2 => $Val2) {
                            if (!is_numeric($Key2)) {
                                if ($Key2 == "Total Income" || $Key2 == "Total Cost of Sales" || $key2 == "Total Operating Expenses" || $Key2 == "Total Non-operating Income" || $Key2 == "Total Non-operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conMTDsearch($Array[$Key][$Key1], $Find, $startDate);
                                } else if ($Key2 == "Total Operating Expenses") {
                                    $Array[$Key]['summaryRow'] = $this->conMTDsearch($Array[$Key][$Key1], $Find, $startDate);
                                }
                            } else {
                                if (strtotime($Array[$Key][$Key1][$Key2]['date']) == strtotime($startDate)) {
                                    $amount = $Array[$Key][$Key1][$Key2]['amount'];
                                    $Array[$Key][$Key1]['total'] = "$amount";
                                    unset($Array[$Key][$Key1][$Key2]);
                                } else {
                                    // $amount = $Array[$Key][$Key1][$Key2]['amount'];
                                    // $Array[$Key][$Key1]['total'] = "$amount";
                                    unset($Array[$Key][$Key1][$Key2]);
                                }
                            }
                        }
                    }
                } else {
                    $Array[$Key] = $this->conMTDsearch($Array[$Key], $Find, $startDate);
                }
            }
        }
        unset($Array['Operating Expenses']);
        return $Array;
    }

    public function conMTDEMPsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) == strtotime($startDate)) {
                            $amount = $Array[$Key][$Key1]['amount'];
                            $Array[$Key]['total'] = "$amount";
                        } else {
                            unset($Array[$Key][$Key1]);
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conMTDEMPTotalsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key][$Key1]);
                        } else {
                            $amount = $Array[$Key][$Key1]['amount'];
                            $Array[$Key]['total'] = "$amount";
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYEMPsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                $amount = 0;
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key][$Key1]);
                        } else {
                            $amount += $Array[$Key][$Key1]['amount'];
                            $Array[$Key]['total'] = "$amount";
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYEMPByEndDate($Array, $Find, $startDate, $endDate)
    {
        if (is_array($Array)) {
            foreach ($Array as $Key => $Val) {
                $amount = 0;
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key][$Key1]);
                        } else {
                            if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($endDate)) {
                                $amount += $Array[$Key][$Key1]['amount'];
                                $Array[$Key]['total'] = "$amount";
                            } else {
                                unset($Array[$Key][$Key1]);
                            }

                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conMTDsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) == strtotime($startDate)) {
                            $amount = $Array[$Key][$Key1]['amount'];
                            $Array[$Key]['total'] = "$amount";
                        } else {
                            unset($Array[$Key][$Key1]);
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key][$Key1]);
                        } else {
                            $amount += $Array[$Key][$Key1]['amount'];
                            $Array[$Key]['total'] = "$amount";
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYByEndDate($Array, $Find, $startDate, $endDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    foreach ($Val as $Key1 => $Val1) {
                        if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($startDate)) {
                            unset($Array[$Key][$Key1]);
                        } else {
                            if (strtotime($Array[$Key][$Key1]['date']) <= strtotime($endDate)) {
                                $amount += $Array[$Key][$Key1]['amount'];
                                $Array[$Key]['total'] = "$amount";
                            } else {
                                unset($Array[$Key][$Key1]);
                            }
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function conMTDProfitsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if (strtotime($Array[$Key]['date']) == strtotime($startDate)) {
                        $amount = $Array[$Key]['amount'];
                        $Array['total'] = "$amount";
                    } else {
                        unset($Array[$Key]);
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYProfitsearch($Array, $Find, $startDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if (strtotime($Array[$Key]['date']) <= strtotime($startDate)) {
                        unset($Array[$Key]);
                    } else {
                        $amount += $Array[$Key]['amount'];
                        $Array['total'] = "$amount";
                    }
                }
            }
        }
        return $Array;
    }

    public function conYTDandYTYProfitByEndDate($Array, $Find, $startDate, $endDate)
    {
        if (is_array($Array)) {
            $amount = 0;
            foreach ($Array as $Key => $Val) {
                if (is_array($Array[$Key])) {
                    if (strtotime($Array[$Key]['date']) <= strtotime($startDate)) {
                        unset($Array[$Key]);
                    } else {
                        if (strtotime($Array[$Key]['date']) <= strtotime($endDate)) {
                            $amount += $Array[$Key]['amount'];
                            $Array['total'] = "$amount";
                        } else {
                            unset($Array[$Key]);
                        }
                    }
                }
            }
        }
        return $Array;
    }

    public function getBudgetByKey($firstKey, $secondkey, $budget)
    {
        if(isset($budget['Operating Expenses'])){
            $data = $budget['Operating Expenses'];
            unset($budget['Operating Expenses']);
            $budget['Less Operating Expenses'] = $data;
        }
        
        foreach ($budget as $budgetKey => $budgetValue) {
            if ($budgetKey == $firstKey) {
                $budgetAmt = $budgetValue[$secondkey];
            }
        }
        return $budgetAmt;
    }

    public function removeBudget($array1, $array2){
        foreach($array1 as $key => $val1){
            $array1Data[] = $key;
        }
        foreach($array2 as $key1 => $val1){
            $array2Data[] = $key1;
        }
        $data = array_diff($array2Data, $array1Data);
        return $data;
    }

    public function checkArray($finalConsData, $budget){

      
       
        $Income = $this->removeBudget($finalConsData['Income'], $budget['Income']);
        foreach($Income as $key => $val){
            $value['Income'][$val] = array('total' => 0);
        }
        $LessCostofSales = $this->removeBudget($finalConsData['Less Cost of Sales'], $budget['Less Cost of Sales']);
        foreach($LessCostofSales as $key => $val){
            $value['Less Cost of Sales'][$val] = array('total' => 0);
        }
        if($budget['Less Operating Expenses']){
            $budgetLessOperExp = $budget['Less Operating Expenses'];
        }else{
            $budgetLessOperExp = $budget['Operating Expenses'];
        }
        $LessOperatingExpenses = $this->removeBudget($finalConsData['Less Operating Expenses'], $budgetLessOperExp);
        foreach($LessOperatingExpenses as $key => $val){
            $value['Less Operating Expenses'][$val] = array('total' => 0);
        }

        $EmployeeCompensation = $this->removeBudget($finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'], $budget['Employee Compensation & Related Expenses']);
        foreach($EmployeeCompensation as $key => $val){
            $value['Less Operating Expenses']['Employee Compensation & Related Expenses'][$val] = array('total' => 0);
        }

        $NonoperatingIncome=  $this->removeBudget($finalConsData['Non-operating Income'], $budget['Non-operating Income']);
        foreach($NonoperatingIncome as $key => $val){
            $value['Non-operating Income'][$val] = array('total' => 0);
        }
        $NonoperatingExpenses = $this->removeBudget($finalConsData['Non-operating Expenses'], $budget['Non-operating Expenses']);
        foreach($NonoperatingExpenses as $key => $val){
            $value['Non-operating Expenses'][$val] = array('total' => 0);
        }
        return $value;
    }

    public function consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr)
    {
        $utility = new utility();
        $budget = array();
        if (sizeof($budgetSummarys) > 0) {
            foreach ($budgetSummarys as $ky => $vy) {
                if ($mtd == 1) {
                    $month_name = $utility->getCurrentMonthField(date('m'));

                    if (trim($vy["line_type"]) == "Row") {
                        $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy[$month_name]); // trim(str_replace('$', '', $vy["month_aug"]));
                    } elseif (trim($vy["line_type"]) == "summaryRow") {
                        $budget["summaryRow"][trim($vy["item_head"])] = round($vy[$month_name]);
                    }
                } else if ($mtd == 2) {
                    $ytd_budget_amount = 0;
                    foreach ($consolidate_month_arr as $month_value) {
                        $ytd_budget_amount = $ytd_budget_amount + $vy[$month_value];
                    }
                    $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($ytd_budget_amount);
                } else {
                    $budget[trim($vy["item_head"])][trim($vy["line_item"])] = round($vy["total_values"]);
                }
            }
        }

        $data = $this->checkArray($finalConsData, $budget);

        $finalData = $this->pushTotalByEmptyArray($finalConsData, $data);
        $value = array();
        foreach ($finalData as $firstKey => $first_innerarray) {
            /* gross profit, Operating Profit & Net Profit */
            if ($firstKey == "Gross Profit" || $firstKey == "Operating Profit" || $firstKey == "Net Profit") {
                if (isset($first_innerarray['total'])) {
                    $total = $first_innerarray['total'];
                } else {
                    $total = 0;
                }
                $value[$firstKey][$firstKey]['no_expand'] = 2;
                $value[$firstKey][$firstKey][0]['Value'] = $firstKey;
                $value[$firstKey][$firstKey][1]['Value'] = $total;
                $value[$firstKey][$firstKey][2]['Value'] = 0;
                $value[$firstKey][$firstKey][3]['Value'] = round($value[$firstKey][$firstKey][1]['Value'] - $value[$firstKey][$firstKey][2]['Value']);
                if ($value[$firstKey][$firstKey][2]['Value'] != 0 || $value[$firstKey][$firstKey][2]['Value'] != '0.0') {
                    $value[$firstKey][$firstKey][4]['Value'] = round(($value[$firstKey][$firstKey][3]['Value'] / $value[$firstKey][$firstKey][2]['Value']) * 100);
                } else {
                    $value[$firstKey][$firstKey][4]['Value'] = 0;
                }
            } else {
                foreach ($finalData[$firstKey] as $secondkey => $second_innerarray) {
                    if ($secondkey == "summaryRow") {
                        foreach ($second_innerarray as $thirdKey => $third_innerarray) {
                            if (isset($third_innerarray['total'])) {
                                $total = $third_innerarray['total'];
                            } else {
                                $total = 0;
                            }
                            $value[$firstKey][$secondkey][$thirdKey][0]['Value'] = $thirdKey;
                            $value[$firstKey][$secondkey][$thirdKey][1]['Value'] = $total;
                            $value[$firstKey][$secondkey][$thirdKey][2]['Value'] = 0;

                            $value[$firstKey][$secondkey][$thirdKey][3]['Value'] = round($value[$firstKey][$secondkey][$thirdKey][1]['Value'] - $value[$firstKey][$secondkey][$thirdKey][2]['Value']);
                            if ($value[$firstKey][$secondkey][$thirdKey][2]['Value'] != 0 || $value[$firstKey][$secondkey][$thirdKey][2]['Value'] != '0.0') {
                                $value[$firstKey][$secondKey][$thirdKey][4]['Value'] = round(($value[$firstKey][$secondKey][$thirdKey][3]['Value'] / $value[$firstKey][$secondKey][$thirdKey][2]['Value']) * 100);
                            } else {
                                $value[$firstKey][$secondkey][$thirdKey][4]['Value'] = 0;
                            }
                        }
                    } else { 
                        foreach ($finalData[$firstKey][$secondkey] as $sec_innerKey => $sec_innerValue) {
                            $getBudgetByKey = $this->getBudgetByKey($firstKey, $secondkey, $budget);
                            if (!is_array($sec_innerValue)) {
                                if (isset($second_innerarray['total'])) {
                                    $total = $second_innerarray['total'];
                                } else {
                                    $total = 0;
                                }

                                if($total != 0 || $getBudgetByKey != 0){
                                    $value[$firstKey][$secondkey][0]['Value'] = $secondkey;
                                    $value[$firstKey][$secondkey][1]['Value'] = $total;
                                    if ($getBudgetByKey != "") {
                                        $value[$firstKey][$secondkey][2]['Value'] = $getBudgetByKey;
                                    } else {
                                        $value[$firstKey][$secondkey][2]['Value'] = 0;
                                    }
                                    $value[$firstKey][$secondkey][3]['Value'] = round($value[$firstKey][$secondkey][1]['Value'] - $value[$firstKey][$secondkey][2]['Value']);
                                    if ($value[$firstKey][$secondkey][2]['Value'] != 0 || $value[$firstKey][$secondkey][2]['Value'] != '0.0') {
                                        $value[$firstKey][$secondkey][4]['Value'] = round(($value[$firstKey][$secondkey][3]['Value'] / $value[$firstKey][$secondkey][2]['Value']) * 100);
                                    } else {
                                        $value[$firstKey][$secondkey][4]['Value'] = 0;
                                    }
                                }
                            } else {
                                /* employee */
                                if ($sec_innerKey != "summaryRow") {
                                    $getBudgetByKey = $this->getBudgetByKey($secondkey, $sec_innerKey, $budget);
                                    if (isset($sec_innerValue['total'])) {
                                        $total = $sec_innerValue['total'];
                                    } else {
                                        $total = 0;
                                    }
                                    if($total != 0 || $getBudgetByKey != 0){
                                        $value[$firstKey][$secondkey][$sec_innerKey][0]['Value'] = $sec_innerKey;
                                        $value[$firstKey][$secondkey][$sec_innerKey][1]['Value'] = $total;
                                        if ($getBudgetByKey != "") {
                                            $value[$firstKey][$secondkey][$sec_innerKey][2]['Value'] = $getBudgetByKey;
                                        } else {
                                            $value[$firstKey][$secondkey][$sec_innerKey][2]['Value'] = 0;
                                        }
                                        
                                        $value[$firstKey][$secondkey][$sec_innerKey][3]['Value'] = round($value[$firstKey][$secondkey][$sec_innerKey][1]['Value'] - $value[$firstKey][$secondkey][$sec_innerKey][2]['Value']);
                                        if ($value[$firstKey][$secondkey][$sec_innerKey][2]['Value'] != 0 || $value[$firstKey][$secondkey][$sec_innerKey][2]['Value'] != '0.0') {
                                            $value[$firstKey][$secondkey][$sec_innerKey][4]['Value'] = round(($value[$firstKey][$secondkey][$sec_innerKey][3]['Value'] / $value[$firstKey][$secondkey][$sec_innerKey][2]['Value']) * 100);
                                        } else {
                                            $value[$firstKey][$secondkey][$sec_innerKey][4]['Value'] = 0;
                                        }
                                    }
                                } else {
                                    foreach ($sec_innerValue as $totalempKey => $totalempValue) {
                                        if (isset($totalempValue['total'])) {
                                            $total = $totalempValue['total'];
                                        } else {
                                            $total = 0;
                                        }
                                        $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][0]['Value'] = $totalempKey;
                                        $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][1]['Value'] = $total;
                                        $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][2]['Value'] = 0;
                                        $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][3]['Value'] = round($value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][1]['Value'] - $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][2]['Value']);
                                        if ($value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][2]['Value'] != 0 || $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][2]['Value'] != '0.0') {
                                            $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][4]['Value'] = round(($value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][3]['Value'] / $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][2]['Value']) * 100);
                                        } else {
                                            $value[$firstKey][$secondkey][$sec_innerKey][$totalempKey][4]['Value'] = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $value;
    }

    public function getAllGroupCategories($searchItems)
    {
        try {
            $response = array();

            $sql = 'SELECT cs.sub_category_id, sub_category_name, ccm.client_master_id
            FROM client_category_master ccm
            JOIN category_subcategory cs ON cs.client_category_id = ccm.client_category_id
            JOIN track_client_sub_category tcm ON tcm.client_id = ccm.client_id and tcm.client_master_id = ccm.client_master_id and tcm.category_sub_id = cs.category_sub_id  WHERE ccm.client_id = 89  AND ccm.client_master_id IN(46, 55, 56, 57, 71)  ORDER BY sub_category_name ASC ';

            $response = array();
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $tmp = array();
                    $is_all_exist = false;
                    $all = array();
                    $i = 0;
                    $stmt->bind_result($sub_category_id, $sub_category_name, $client_master_id);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["sub_category_id"] = $sub_category_id;
                        $tmp["sub_category_name"] = $sub_category_name;
                        $tmp["client_master_id"] = $client_master_id;

                        $report_dir_path = __DIR__ . "/plreport/" . $searchItems['client_master_id'] . "/";
                        $prefix = $searchItems['client_master_id'] . "_plreport_";

                        $sub_category_id2 = 1000;

                        if (!empty($sub_category_id)) {
                            $suffix = "";
                            $suffix .= $sub_category_id;
                            $suffix .= ".json";
                        }
                        $latest_filename = $this->latestFileAtPath($report_dir_path, $prefix, $suffix);
                        $report_path = $report_dir_path . $latest_filename;

                        //$files[] = $report_path;
                        $tmp['path'] = $report_path;

                        $response[] = $tmp;
                    }

                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllGroupTrackingCategories($searchItems)
    {
        try {
            $response = array();

            $sql = 'SELECT  ccm.client_id, cs.category_sub_id, ccm.client_category_id, ccm.category_id, category_name, ccm.is_active, cs.sub_category_id, sub_category_name, ccm.client_master_id
                            FROM client_category_master ccm
                            JOIN category_subcategory cs ON cs.client_category_id = ccm.client_category_id';

            if (sizeof($searchItems) > 0) {
                $sql .= " WHERE ";
            }
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'ccm.client_master_id = ? ';
                        break;
                    case 'category_id':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.category_id = ? ";
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "ccm.is_active = ? ";
                        break;
                    case 'category_sub_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = " ccm.category_sub_id = ? ";
                        break;
                    case 'sub_category_id':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = " cs.sub_category_id = ? ";
                        break;

                }
            }
            $sql .= implode(' AND ', $query);
            $sql .= "  ORDER BY sub_category_name ASC ";
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }
            $response["trackingCategories"] = array();
            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $tmp = array();
                    $is_all_exist = false;
                    $all = array();
                    $i = 0;
                    $stmt->bind_result($client_id, $category_sub_id, $client_category_id, $category_id, $category_name, $is_active, $sub_category_id, $sub_category_name, $client_master_id);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["client_id"] = $client_id;
                        $tmp["category_sub_id"] = $category_sub_id;
                        $tmp["client_category_id"] = $client_category_id;
                        $tmp["category_id"] = $category_id;
                        $tmp["category_name"] = $category_name;
                        $tmp["is_active"] = $is_active;
                        $tmp["sub_category_id"] = $sub_category_id;
                        $tmp["sub_category_name"] = $sub_category_name;
                        $tmp['client_master_id'] = $client_master_id;

                        $report_dir_path = __DIR__ . "/plreport/" . $searchItems['client_master_id'] . "/";
                        $prefix = $searchItems['client_master_id'] . "_plreport_";

                        $sub_category_id2 = 1000;

                        if (!empty($sub_category_id)) {
                            $suffix = "";
                            $suffix .= $sub_category_id;
                            $suffix .= ".json";
                        }
                        $latest_filename = $this->latestFileAtPath($report_dir_path, $prefix, $suffix);
                        $report_path = $report_dir_path . $latest_filename;

                        //$files[] = $report_path;
                        $tmp['path'] = $report_path;

                        if ($searchItems['groupby'] == 1) {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"][$category_name]) <= 1) {
                                    $response["trackingCategories"][$category_name][0] = $tmp;
                                } else {
                                    array_unshift($response["trackingCategories"][$category_name], $tmp);
                                }
                            } else {
                                $response["trackingCategories"][$category_name][] = $tmp;
                            }
                        } else {
                            if (strtolower(trim($sub_category_name)) == strtolower(trim('All'))) {
                                if (sizeof($response["trackingCategories"]) <= 1) {
                                    $response["trackingCategories"][0] = $tmp;
                                } else {
                                    $response["trackingCategories"][] = $tmp;
                                }
                            } else {
                                $response["trackingCategories"][] = $tmp;
                            }
                        }
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function replace_key($find, $replace, $array)
    {
        $arr = array();
        foreach ($array as $key => $value) {
            if ($key == $find) {
                $arr[$replace] = $value;
            } else {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }

    public function syncGroupData($group_id, $client_id, $selected_master_id, $report_start_date, $toCur)
    {
        
        $utility = new utility();
        $dash_db = new dashboardReport();
        $client = new client();

        $catResponse = array();
        $catResponse1 = array();
        foreach ($selected_master_id as $client_master_ids) {
            $client_master_id = $client_master_ids;
            $searchItems = array();
            $searchItems['client_id'] = $client_id;
            $searchItems['client_master_id'] = $client_master_id;
            $searchItems["groupby"] = 1;
            $catResponse[$client_master_id] = $this->getAllGroupTrackingCategories($searchItems);
            /* for write double categories in json file */
            $catResponse1[] = $this->getAllGroupTrackingCategories($searchItems);
        }

        /* budget category*/
        $getBudgetCat = $this->getBudgetCat($catResponse);
        /* company budget details */
        $budgetparams["groupBy"] = 1;
        $totalbudgetSummarys = array();
        $totalbudgetSummarys = $dash_db->getBudgetDetailsFromDB($group_id, $budgetparams);
        /* company pl report month array */
        $consolidate_month_arr = array();
        $startDate = date("Y-m-01", strtotime($report_start_date));
        $endDate = date('Y-m-d');
        while (strtotime($startDate) <= strtotime($endDate)) {
            $consolidate_month_arr[] = $utility->getCurrentMonthField(date('m', strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
        }
        $consolidate_month_arr = array_reverse($consolidate_month_arr);
        /* get single or double category size */
        foreach ($catResponse as $key => $value) {
            if (sizeof($value['trackingCategories']) > 1) {
                if ($value['trackingCategories']['All']) {
                    unset($value['trackingCategories']['All']);
                    unset($value['trackingCategories']['All'][0]);
                }
                $categoryRes[$key] = $value;
            } else {
                $categoryRes[$key] = $value;
            }
        }
        $catSize = 0;
        foreach ($categoryRes as $key => $value) {
            $catSize = sizeof($value['trackingCategories']);
        }

        $getCategories = $this->getConsolidationCategory($catResponse, $catResponse1, $catSize);
        /* file path */
        $unique_files = $getCategories['unique_files'];
        /* json tracking categories */
        $totalFiles = $getCategories['totalFiles'];
        /* get flash report files and eliminated data */
        $getFlashfilesAndEliminatedData = $this->getFlashfilesAndEliminatedDataByMasterId($selected_master_id, $group_id);
        $flash_files = $getFlashfilesAndEliminatedData['flash_files'];
        $trendEliminatedData = $getFlashfilesAndEliminatedData['trendEliminatedData'];
        $blncEliminatedData = $getFlashfilesAndEliminatedData['blncEliminatedData'];
        /* budget summarys */
        $budgetSummarys = $this->getBudgetSummary($unique_files, $getBudgetCat, $totalbudgetSummarys);
        /* financial report */
        if (sizeof($unique_files) > 0) {
            foreach ($unique_files as $filePathKey => $files) {
                $currency_value = $this->getCurrencyValue($files, $toCur);
                $getConsCurrencyValue = $this->getConsCurrencyValue($files, $toCur);

                $jsonMerger = new JSONMerger($files);
                $balanceData = $jsonMerger->balanceSheetIndex($group_id, $blncEliminatedData);
                $tendData = $jsonMerger->trendIndex($group_id, $trendEliminatedData);

                $trendPNL = new TrendPNL($tendData);
                /* get net profit for calculate the current year earings value in balancesheet report */
                $getEBITDA = $this->getEBIDA($trendPNL, "1");
                $getNonOperatingIncome = $this->getEBIDA($trendPNL, "2");
                $getNonOperatingExpenses = $this->getEBIDA($trendPNL, "3");
                $NetProfit = $trendPNL->netProfit1($getEBITDA, $getNonOperatingIncome, $getNonOperatingExpenses);
                unset($NetProfit['no_expand']);
                $collection = collect($NetProfit)->groupBy('year')->toArray();
                $profitArray = array();
                foreach ($collection as $profit) {
                    if (is_array($profit)) {
                        $coll = collect($profit)->sortBy('month')->toArray();
                        $profitArray[] = $coll;
                    }
                }

                $finalNetProfit = collect($profitArray)->collapse()->toArray();
                $netIncome = array();
                $dates = $utility->getDayTillMonthNew($group_id, $report_start_date, null);
                foreach ($dates as $k => $v) {
                    if (is_numeric($k)) {
                        $month = date('n', strtotime($v['firstdate']));
                        $year = date('y', strtotime($v['firstdate']));
                        if ($month != "" && $year != "") {
                            $id = $month . '-' . $year;
                        } else {
                            $id = '';
                        }
                        if ((sizeof($finalNetProfit) > 0)) {
                            $netIncomeKey = $dash_db->searchIdFromArray($month . '-' . $year, $finalNetProfit);

                            if (sizeof($netIncomeKey) > 0) {
                                $netIncome[$k] = array('date' => $finalNetProfit[$netIncomeKey]['date'], 'amount' => $finalNetProfit[$netIncomeKey]['amount'], 'month' => $month, 'year' => $year, 'id' => $id);
                            } else {
                                $netIncome[$k] = array('date' => '-', 'amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                            }
                        }
                    }
                }
                $finalArray = array();
                foreach ($dates as $k => $v) {
                    if (is_numeric($k)) {
                        $month = date('n', strtotime($v['firstdate']));
                        $year = date('y', strtotime($v['firstdate']));
    
                        if ($month != "" && $year != "") {
                            $id = $month . '-' . $year;
                        } else {
                            $id = '';
                        }

                        if (!isset($netIncome[$k - 1])) {
                            $finalArray[$k] = array('amount' => 0, 'month' => $month, 'year' => $year, 'id' => $id);
                        } else {
                            $finalArray[$k] = array('amount' => $netIncome[$k - 1]['amount'], 'month' => $netIncome[$k]['month'], 'year' => $netIncome[$k]['year'], 'id' => $netIncome[$k]['month'] . '-' . $netIncome[$k]['year']);
                        }
                    }
                }
                // $finalArray = array();
                // foreach ($finalNetProfit as $value) {
                //     $value['amount'];
                //     $amount += $value['amount'];
                //     $value['amount'] = $amount;
                //     $finalArray[] = $value;
                // }

                /* operating expenses array data in trendPNL */
                $top_exp = $trendPNL->LessOperatingExpenses();
                $emp_exp = $trendPNL->LessOperatingExpensesAndEmp();

                $finalData = $this->ArraySearch($top_exp, "id", $report_start_date);
                $finalData = $this->ArrayFilter($finalData);
                $finalData = $this->ArraySearchss($finalData);

                $empfinalData = $this->ArraySearch($emp_exp, "id", $report_start_date);
                $empfinalData = $this->ArrayFilter($empfinalData);
                $empfinalData = $this->ArraySearchss($empfinalData);

                $finalData['Total Employee Compensation & Related Expenses'] = $empfinalData['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];
                unset($finalData['Employee Compensation & Related Expenses']);
                unset($finalData['summaryRow']);
                arsort($finalData);
                /* company P&L data */
                $curdate = date('Y-m-t');
                $consData = $this->trendpnlarray($trendPNL, $finalData);
                $finalConsDataMTD = $this->ConsMTDArraySearch($consData, "id", $curdate);
                $mtd = 1;
                $totalbudgetCalMTD = $this->consolidatePNL($finalConsDataMTD, $mtd, $budgetSummarys, $consolidate_month_arr);

                $consolidateMTD = $this->totalbudgetCal($totalbudgetCalMTD);

                $finalConsData = $this->ConsolidateArraySearch($consData, "id", $report_start_date);
                $mtd = 0;
                $totalbudgetCalYTY = $this->consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr);
                $consolidatePNLYTY = $this->totalbudgetCal($totalbudgetCalYTY);

                $mtd = 2;
                $totalbudgetCalYTD = $this->consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr);
                $consolidatePNLYTD = $this->totalbudgetCal($totalbudgetCalYTD);

                $consolidatePNLYTDandMTD = $client->ArrayPush($consolidateMTD, $consolidatePNLYTD);
                /* balancesheet data */
                $balanceSheet = new BalanceSheet($balanceData, $finalArray);
                /* cashflow data */
                $aBlncSheet = $this->balanceSheetArraycash($balanceSheet);
                $aPNLTrend = $this->trendPNLCashFlow($trendPNL, $finalData);
                $finalCashFlow = $this->consolCashFlow($aPNLTrend, $aBlncSheet, $balanceSheet);
                /* generate the final report json files */
                $filterBy = "group";

                $generateConFinancialReport = $this->generateConsolFinancialReport($totalFiles, $filterBy, $files, $balanceSheet,
                    $consolidateMTD, $consolidatePNLYTD, $consolidatePNLYTY, $consolidatePNLYTDandMTD, $trendPNL, $finalData, $finalCashFlow, $toCur,
                    $currency_value, $date, $group_id, $catSize, $filePathKey, $report_start_date);
            }
        }
        /* flash report */
        if (sizeof($flash_files) > 0) {
            /* generate the flash report json files */
            $generateConFlashReport = $this->generateConsolFlashReport($flash_files, $toCur, $group_id);
        }
        /* generate group metric json file */
        $sub_category_id1 = 1000;
        if($catSize == 2){
            $sub_category_id2 = 1000;
        }
        $GenerateGroupMetrics = $this->GenerateGroupMetrics($group_id, $report_start_date, $sub_category_id1, $sub_category_id2);
        return $response;
    }

    public function getBudgetCat($catResponse)
    {
        if (count($catResponse) > 0) {
            foreach ($catResponse as $key => $value) {
                $groupParam = array();
                foreach ($value["trackingCategories"] as $k => $v) {
                    foreach ($v as $k1 => $v1) {
                        if ($v1["category_name"] != "All") {
                            $groupParam[$v1["category_name"]][] = $v1["sub_category_id"];
                        }
                    }
                }
                $mParams = array();
                $finalPath = array();
                if (sizeof($groupParam) == 0 && sizeof($value) > 0) {
                    $dir = __DIR__ . "/plreport/" . $client_master_id . "/";
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    $finalPath[] = $dir . $client_master_id . "_plreport_" . $loginDate . "_" . TERRITORY_ID . ".json";
                    $mParams["sub_category_id1"][] = TERRITORY_ID;
                } else if (sizeof($groupParam) > 0) {
                    $dir = __DIR__ . "/plreport/" . $client_master_id . "/";
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777, true);
                    }
                    switch (sizeof($groupParam)) {
                        case 1:
                            $arrayKeys = array_keys($groupParam);
                            $array1 = $groupParam[$arrayKeys[0]];
                            foreach ($array1 as $k2 => $v2) {
                                $mParams["sub_category_id1"][] = $v2;
                                $path = $dir . $client_master_id . "_plreport_" . $loginDate;
                                $path .= "_" . $v2;
                                $path .= ".json";
                                $finalPath[] = $path;
                            }
                            $mParams["sub_category_id1"][] = TERRITORY_ID;
                            break;
                        case 2:
                            $arrayKeys = array_keys($groupParam);
                            $array1 = $groupParam[$arrayKeys[0]];
                            $array2 = $groupParam[$arrayKeys[1]];

                            foreach ($array1 as $k2 => $v2) {
                                foreach ($array2 as $k3 => $v3) {
                                    $mParams["sub_category_id1"][] = $v2;
                                    $mParams["sub_category_id2"][] = $v3;
                                    $path = $dir . $client_master_id . "_plreport_" . $loginDate;
                                    $path .= "_" . $v2;
                                    $path .= "_" . $v3;
                                    $path .= ".json";
                                    $finalPath[] = $path;
                                }
                            }
                            $mParams["sub_category_id1"][] = TERRITORY_ID;
                            $mParams["sub_category_id2"][] = TERRITORY_ID;
                            break;
                        case 3:
                            break;
                    }
                }
            }
            return $mParams;
        }
    }

    public function getConsolidationCategory($catResponse, $catResponse1, $catSize)
    {
        $dash_db = new dashboardReport();
        $ArrayData = array();
        if ($catSize == 2) {
            /* get all file path */
            $categoryData = $dash_db->getAllGroupCat($catResponse1);
            foreach ($categoryData['diff_files'] as $key => $value) {
                $unique_files[$key] = $value;
            }
            /* get first and second category */
            $jsonCategory = array();
            foreach ($catResponse as $key1 => $responseValue) {
                $jsonCategory1[] = array();
                foreach ($responseValue as $key2 => $categoryValue) {
                    $jsonCategory[] = current($categoryValue);
                    $jsonCategory1[] = end($categoryValue);
                }
            }
            /* first category pushed to Category 1 */
            $firstCategory = array();
            $firstCategory[] = array('sub_category_id' => 1000, 'sub_category_name' => 'All');
            foreach ($jsonCategory as $key => $categoryFirstArray) {
                foreach ($categoryFirstArray as $Arraykey => $categoryFirstValue) {
                    if ($categoryFirstValue['sub_category_name'] != "All") {
                        $firstCategory[] = $categoryFirstValue;
                    }
                }
            }
            $finalFirstCategory['Category1'] = $firstCategory;
            /* second category pushed to Category 2 */
            $secondCategory = array();
            $secondCategory[] = array('sub_category_id' => 1000, 'sub_category_name' => 'All');
            foreach ($jsonCategory1 as $key => $categoryFirstArray) {
                foreach ($categoryFirstArray as $Arraykey => $categoryFirstValue) {
                    if ($categoryFirstValue['sub_category_name'] != "All") {
                        $secondCategory[] = $categoryFirstValue;
                    }
                }
            }
            $finalSecCategory['Category2'] = $secondCategory;
            /* merged firsrt and second category */
            $totalFilesArray = array_merge($finalFirstCategory, $finalSecCategory);
            foreach ($totalFilesArray as $key => $files) {
                foreach ($files as $key1 => $files1) {
                    $finalCat[$key][$files1['sub_category_name']] = $files1;
                }
            }
            foreach ($finalCat as $key => $files) {
                foreach ($files as $key1 => $files1) {
                    $finalCategories[$key][] = $files1;
                }
            }
            $totalFiles = $finalCategories;
            $ArrayData = array('unique_files' => $unique_files, "totalFiles" => $totalFiles);
        } else if ($catSize == 1) {
            $unique_files = array();
            $diff_ids = array();
            foreach ($catResponse as $key1 => $responseValue) {
                foreach ($responseValue as $key2 => $categoryValue) {
                    foreach ($categoryValue as $key3 => $departmentValue) {
                        $unique_files[$departmentValue[0]['sub_category_name']][] = $departmentValue[0]['path'];
                        unset($responseValue[$key2][$key3][0]);
                        foreach ($responseValue[$key2][$key3] as $key4 => $finalValue) {
                            if (array_unique($responseValue[$key2][$key3])) {
                                $data = array_unique($finalValue);
                                $unique_files[$responseValue[$key2][$key3][$key4]['sub_category_name']][] = $data['path'];
                            } else {
                            }
                            $diff_ids[$responseValue[$key2][$key3][$key4]['sub_category_name']] = $responseValue[$key2][$key3][$key4];
                        }
                    }
                }
            }

            $total_cat[] = array('sub_category_id' => 1000, 'sub_category_name' => 'All');
            foreach ($diff_ids as $key => $value) {
                $total_cat[] = $value;
            }
            $totalFiles['Category1'] = $total_cat;
            $ArrayData = array('unique_files' => $unique_files, "totalFiles" => $totalFiles);
        }

        return $ArrayData;
    }

    public function getFlashfilesAndEliminatedDataByMasterId($selected_master_id, $group_id)
    {
        $pushArr = array();
        foreach ($selected_master_id as $client_master_ids) {
            $client_master_id = $client_master_ids;
            $flash_report_dir_path = __DIR__ . "/flashreport/" . $client_master_id . "/";
            $flash_prefix = $client_master_id . "_flashreport_";
            $flash_suffix = "";
            $flash_latest_filename = $this->latestFileAtPath($flash_report_dir_path, $flash_prefix, $flash_suffix);
            $flash_report_path = $flash_report_dir_path . $flash_latest_filename;
            $flash_files[] = $flash_report_path;

            /** get eliminate key for trend */
            $trend_sql = "SELECT gcac.group_id, gcac.client_master_id, gcac.account_name, gcac.account_category, gcac.is_eliminated FROM group_companies_account_categories as gcac WHERE gcac.group_id = $group_id AND client_master_id = $client_master_id AND is_eliminated = 1 AND account_category = 1";
            if ($stmt1 = $this->conn->prepare($trend_sql)) {
                $stmt1->execute();
                $stmt1->bind_result($group_id, $client_master_id, $account_name, $account_category, $is_eliminated);
                $stmt1->store_result();
                if ($stmt1->num_rows == 0) {
                    $trend_sql_data[] = array();
                } else {
                    $pushArr1 = array();
                    while ($result = $stmt1->fetch()) {
                        $pushArr1[] = array('account_name' => $account_name, 'is_eliminated' => $is_eliminated, 'group_id' => $group_id, 'account_category' => $account_category, 'client_master_id' => $client_master_id, 'report_path' => $report_path);
                    }
                    $trend_sql_data[] = $pushArr1;
                }
            } else {
                $trend_sql_data = [];
            }

            /** get eliminate key for balancesheet */
            $blnc_sql = "SELECT gcac.group_id, gcac.client_master_id, gcac.account_name, gcac.account_category, gcac.is_eliminated FROM group_companies_account_categories as gcac WHERE gcac.group_id = $group_id AND client_master_id = $client_master_id AND is_eliminated = 1 AND account_category = 2 ORDER BY  gcac.client_master_id";
            if ($stmt = $this->conn->prepare($blnc_sql)) {
                $stmt->execute();
                $stmt->bind_result($group_id, $client_master_id, $account_name, $account_category, $is_eliminated);
                $stmt->store_result();
                if ($stmt->num_rows == 0) {
                    $blnc_sql_data[] = array();
                } else {
                    $pushArr = array();
                    while ($result = $stmt->fetch()) {
                        $pushArr[] = array('account_name' => $account_name, 'is_eliminated' => $is_eliminated, 'group_id' => $group_id, 'account_category' => $account_category, 'client_master_id' => $client_master_id, 'report_path' => $report_path);
                    }
                    $blnc_sql_data[] = $pushArr;
                }
            } else {
                $blnc_sql_data = [];
            }
        }
        $arrayData = array('flash_files' => $flash_files, 'trendEliminatedData' => $trend_sql_data, 'blncEliminatedData' => $blnc_sql_data);
        return $arrayData;
    }

    public function getBudgetSummary($unique_files, $getBudgetCat, $totalbudgetSummarys)
    {
        for ($i = 0; $i < count($unique_files); $i++) {
            if (isset($getBudgetCat["sub_category_id1"])) {
                $params["sub_category_id1"][] = $getBudgetCat["sub_category_id1"][$i];
            }
            if (isset($getBudgetCat["sub_category_id2"])) {
                $params["sub_category_id2"][] = $getBudgetCat["sub_category_id2"][$i];
            }
        }
        $params["sub_category_id1"][] = 1000;
        if (isset($params["sub_category_id2"])) {
            $params["sub_category_id2"][] = 1000;
        }

        foreach ($params['sub_category_id1'] as $paramValue) {
            $budgetSummarys = array();
            if (sizeof($totalbudgetSummarys["budgetSummary"]) > 0) {
                foreach ($totalbudgetSummarys["budgetSummary"] as $ky => $vy) {
                    if ($vy["territory_id"] == $paramValue) {
                        array_push($budgetSummarys, $vy);
                    }
                }
            }
        }

        foreach ($params['sub_category_id2'] as $paramValue) {
            $budgetSummarys = array();
            if (sizeof($totalbudgetSummarys["budgetSummary"]) > 0) {
                foreach ($totalbudgetSummarys["budgetSummary"] as $ky => $vy) {
                    if (isset($paramValue["sub_category_id2"])) {
                        if ($vy["brand_id"] == $paramValue["sub_category_id2"]) {
                            array_push($budgetSummarys, $vy);
                        }
                    }
                }
            }
        }
        return $budgetSummarys;
    }

    public function getCurrencyValue($files, $toCur)
    {
        foreach ($files as $filePath) {
            $json = json_decode(file_get_contents($filePath), true);
            if ($json['default_currency'] !== $toCur) {
                $context = stream_context_create(array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
                    ),
                ));
                $data = file_get_contents(CURRENCY_URL . '/?from=' . $toCur . '&to=' . $json['default_currency'] . '&amount=1', false, $context);
                $data = json_decode($data, true);
                $currency[] = $data['to'][0]['mid'];
                //$currency = [1, 1, 1, 1, 1, 1, 1];
                $jsondata = $currency;
            } else {
                $currency[] = 1;
                $jsondata = $currency;
            }
        }
        return $jsondata;
    }

    public function getConsCurrencyValue($files, $toCur)
    {
        foreach ($files as $filePath) {
            $json = json_decode(file_get_contents($filePath), true);
            if ($json['default_currency'] !== $toCur) {
                $startDate = $json['report_start_date'];
                $endDate = date('Y-m-t');
                $context = stream_context_create(array(
                    'http' => array(
                        'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
                    ),
                ));

                $data = file_get_contents(CONSOL_HISTORIC_CURRENCY_URL . '/?from=' . $toCur . '&to=' . $json['default_currency'] . '&start_timestamp=' . $endDate . '&end_timestamp=' . $startDate . '&amount=1', false, $context);
                $data = json_decode($data, true);
                $currency[] = $data['stats'][0]['average'];
                //$currency = [1, 1, 1, 1, 1, 1, 1];
                $jsondata = $currency;
            } else {
                $currency[] = 1;
                $jsondata = $currency;
            }
        }
        return $jsondata;
    }

    public function generateConsolFinancialReport($totalFiles, $filterBy, $files, $balanceSheet, $consolidateMTD, $consolidatePNLYTD,
        $consolidatePNLYTY, $consolidatePNLYTDandMTD, $trendPNL, $finalData, $finalCashFlow, $toCur, $currency_value,
        $date, $group_id, $catSize, $filePathKey, $report_start_date, $sub_category_id1 = null, $sub_category_id2 = null) {
        /**Merge Array **/
        $totalArray = "";
        $totalArray .= "{";
        /**trackingcategories */
        $getTrackingCategory = $this->getTrackingCategory($totalFiles, $filterBy);
        $totalArray .= $getTrackingCategory;
        /**balancesheet */
        $balancesheetArray = $this->getBalancesheetReportValues($files, $balanceSheet);

        $totalArray .= $balancesheetArray;
        /**consolidate */
        /**mtd */
        $MTDReportVAlus = $this->consolidateMTDFinalReport($consolidateMTD);
        $final_array = "";
        $final_array .= $MTDReportVAlus;

        /**YTD */
        $YTDReportVAlus = $this->consolidateYTDFinalReport($consolidatePNLYTD);
        $final_array .= $YTDReportVAlus;
        /**YTY */
        $YTYReportVAlus = $this->consolidateYTYFinalReport($consolidatePNLYTY);
        $final_array .= $YTYReportVAlus;
        /**YTY_YTD */
        $YTDMTDReportVAlus = $this->consolidateYTDMTDFinalReport($consolidatePNLYTDandMTD);
        $final_array .= $YTDMTDReportVAlus;
        $totalArray .= $final_array;
        /**trendPNL */
        $trendpnlarray = $this->getTrendFinalReport($trendPNL, $finalData);
        $totalArray .= $trendpnlarray;
        /** */
        /**currencyList */
        $getCurrencyListArray = $this->getCurrencyList();
        $totalArray .= $getCurrencyListArray;
        /**cashFlowStatement */
        $cashflow_finalArray = $this->cashflowFinalReport($finalCashFlow);
        $totalArray .= $cashflow_finalArray;
        /**invoiceByName */
        $invoiceByName_finalArray = $this->getinvoiceByNameReport();
        $totalArray .= $invoiceByName_finalArray;
        /**Currency conversion */
        $invoiceByName_finalArray = $this->currencyConversion($files, $toCur, $currency_value);
        $totalArray .= $invoiceByName_finalArray;
        $totalArray .= ',';

        $date = date("Y-m-d H:i:s");
        $totalArray .= '"last_updated_time":' . '"' . $date . '"';
        $totalArray .= ',';
        $totalArray .= '"default_currency":' . '"' . $toCur . '"';
        $totalArray .= '}';
        /** generate plreport json files*/
        if ($filterBy == "group") {
            if (!file_exists(BASE_PATH . '/plreport')) {
                mkdir(BASE_PATH . '/plreport', 0777, true);
            }
            if (!file_exists(BASE_PATH . '/plreport/' . $group_id)) {
                mkdir(BASE_PATH . '/plreport/' . $group_id, 0777, true);
            }

            if ($catSize == 2) {
                if ($filePathKey == "All_All") {
                    $filePathKey = "1000_1000";
                }
                $file_name = $group_id . '_plreport_' . $report_start_date . '_' . mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filePathKey) . '.json';
                $path = BASE_PATH . '/plreport/' . $group_id . '/' . $file_name;
            } else if ($catSize == 1) {
                if ($filePathKey == "All") {
                    $filePathKey = 1000;
                }
                $file_name = $group_id . '_plreport_' . $report_start_date . '_' . mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filePathKey) . '.json';
                $path = BASE_PATH . '/plreport/' . $group_id . '/' . $file_name;
            }
            $response = file_put_contents($path, $totalArray);
        } else if ($filterBy == "SH") {
            if (!file_exists(BASE_PATH . '/shareholder')) {
                mkdir(BASE_PATH . '/shareholder', 0777, true);
            }
            if (!file_exists(BASE_PATH . '/shareholder/' . $group_id)) {
                mkdir(BASE_PATH . '/shareholder/' . $group_id, 0777, true);
            }

            if ($sub_category_id2 > 0) {
                $sub_id2 = "_" . $sub_category_id2;
            }

            $Curdate = date("Y-m-d");
            $file_name = $group_id . '_shareholder_' . $Curdate . '_' . $sub_category_id1 . $sub_id2 . '.json';
            $path = BASE_PATH . '/shareholder/' . $group_id . '/' . $file_name;
            $data = file_put_contents($path, $totalArray);
            $response = array();
            $response['error'] = false;
            $response['message'] = SYNC_SH;
        }

        return $response;
    }

    public function consolCashFlow($aPNLTrend, $aBlncSheet, $balanceSheet)
    {
        $dash_db = new dashboardReport();
        $startDate = date("Y-m-01", strtotime("-12 months"));
        $endDate = date('Y-m-d');
        $i = 0;
        while (strtotime($startDate) <= strtotime($endDate)) {
            $dates[] = array("firstdate" => date("Y-m-01", strtotime($startDate)), "lastdate" => date("Y-m-t", strtotime($startDate)));
            $startDate = date('Y-m-01', strtotime($startDate . '+ 1 month'));
            $i++;
        }

        $cashFlow = $dash_db->getGroupCashFlowStatement($aPNLTrend, $aBlncSheet, "", $dates);
        $BlncData = $this->balanceSheetArray($balanceSheet);
        $cashInBankData = $BlncData['Bank']['Cash in Bank'];
        $netCashNetOfPeriod = $cashFlow['Cash and Cash Equivalents']['summaryRow']['value'];
        $OtherMovementsInWorkingCapital = $balanceSheet->AdjustmentValue($cashInBankData, $netCashNetOfPeriod);

        unset($cashFlow['Cash Flows from Operating Activities'][5]);
        $cashFlow['Cash Flows from Operating Activities'][5] = ['title' => "Other Movements in Working Capital", "value" => $OtherMovementsInWorkingCapital];
        $cashInFlow = new CashInFlow($cashFlow);
        $finalCashFlow = $cashInFlow->CashFlow();

        $opValue = $finalCashFlow['Cash Flows from Operating Activities']['summaryRow']['value'];
        $InValue = $finalCashFlow['Cash Flows from Investing Activities']['summaryRow']['value'];
        $cashValue = $finalCashFlow['Cash Flows from Financial Activities']['summaryRow']['value'];

        $netDecreaseValue = $cashInFlow->netDecreaseValue($opValue, $InValue, $cashValue);
        unset($cashFlow['Cash and Cash Equivalents'][0]);
        $finalCashFlow['Cash and Cash Equivalents'][0] = ['title' => "Net Increase (decrease) in cash & cash equivalents", "value" => $netDecreaseValue];

        /*Cash and Cash Equivalents  */
        $CashEquivalents = $finalCashFlow['Cash and Cash Equivalents'];
        $evalue = array();
        foreach ($CashEquivalents as $key => $equivalentValue) {
            if (is_numeric($key)) {
                $evalue[] = $equivalentValue['value'];

            }
        }
        $CashEquivalentsData = $cashInFlow->Totalcashequivalents($evalue);
        unset($CashEquivalents['summaryRow']);
        $finalCashFlow['Cash and Cash Equivalents'] = array_merge($CashEquivalents, $CashEquivalentsData);
        return $finalCashFlow;
    }

    public function generateConsolFlashReport($flash_files, $toCur, $group_id)
    {
        $flashCur = $this->getFlashCurrByCurDate($flash_files, $toCur);
        $flashmerger = new flashMerger($flash_files);

        $mergerValues = $flashmerger->flashIndex($group_id, $flashCur);
        $receivableValues = $flashmerger->flashRecivableIndex($group_id, $flashCur);
        $jsonValues = $flashmerger->flashBankTransactionIndex($group_id, $flashCur);

        $estijsonValues = $flashmerger->flashEstimatePayrollIndex($group_id, $flashCur);

        $bankTransactionSummary = $flashmerger->bankTransaction($jsonValues);
        $payable = $flashmerger->Agedpayables($mergerValues);
        $recivable = $flashmerger->Agedrecivables($receivableValues);
        $bankTransaction = $flashmerger->BankRowValues($jsonValues);
        $estimatedPayrollValue = $flashmerger->EstimatepayRoll($estijsonValues, $jsonValues);
        if ($estimatedPayrollValue['estimatedPayroll']) {
            $estimatedPayroll = $estimatedPayrollValue['estimatedPayroll'];
        } else {
            $estimatedPayroll = $estimatedPayrollValue;
        }

        $totalEmpComp['Total Employee Compensation & Related Expenses'] = $estimatedPayroll['Total Employee Compensation & Related Expenses'];
        unset($estimatedPayroll[1]);
        unset($estimatedPayroll[2]);
        unset($estimatedPayroll[3]);
        unset($estimatedPayroll[4]);
        unset($estimatedPayroll[5]);
	unset($estimatedPayroll[6]);
        unset($estimatedPayroll[7]);
        unset($estimatedPayroll[8]);
        unset($estimatedPayroll[9]);
        unset($estimatedPayroll[10]);
        unset($estimatedPayroll['Total Employee Compensation & Related Expenses']);
        $finalEstimatedPayroll = array_merge($estimatedPayroll, $totalEmpComp);

        $accountsSummary = $flashmerger->Accountsummary($jsonValues);
        /*  flash report - json merger */
        $finalFlashRecord = $flashmerger->finalFlashRecord($accountsSummary, $payable, $recivable, $finalEstimatedPayroll, $bankTransaction, $bankTransactionSummary, $group_id, $toCur);
    }

    public function getMasterIdByGroupId($group_id)
    {
        try {
            $sql = "SELECT GROUP_CONCAT(distinct mcl.client_master_id) as client_master_id, cgm.group_id, cgm.client_id, cmd.company_name as group_name, cmd.default_currency, cmd.report_start_date FROM
                        client_master_detail as  cmd
                        JOIN client_group_mapping as cgm on cgm.group_id = cmd.client_master_id
                        JOIN master_groups_company_list as mcl on mcl.group_id = cgm.group_id
                        where cmd.is_group = 1 and cgm.group_id = $group_id";

            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->execute();
                $stmt->bind_result($client_master_id, $group_id, $client_id, $group_name, $default_currency, $report_start_date);
                $stmt->store_result();
                $pushArr = array();
                $group = array();
                if ($stmt->num_rows > 0) {
                    while ($result = $stmt->fetch()) {
                        $temp['group_id'] = $group_id;
                        $temp['client_id'] = $client_id;
                        $temp['group_name'] = $group_name;
                        $temp['default_currency'] = $default_currency;
                        $temp['report_start_date'] = $report_start_date;
                        $master_id_array = explode(',', $client_master_id);
                        $temp['group_details'] = $master_id_array;
                        $group = $temp;
                    }
                    $response["error"] = false;
                    $response['result'] = $group;
                } else {
                    $response["error"] = true;
                    $response['result'] = NO_COMPANY_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteFolderByGroupId($dir)
    {
        if (is_dir($dir)) {
            $files = array();
            if ($handle = opendir($dir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $files[] = $file;
                    }
                    if (count($files) >= 1) {
                        break;
                    }
                }
                closedir($handle);
            }
            foreach ($files as $file) {
                $path_user = $dir . '/' . $file;
                if (chmod($path_user, 0777)) {
                    if (file_exists($path_user)) {
                        unlink($path_user);
                    }
                }
                rmdir($dir);
            }
        }
    }

    public function checkYearOfTrend($trendcurrency_type, $fromCur, $toCur, $month, $year, $byear)
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
            ),
        ));

        $utility = new utility();
        $month_field = $utility->getCurrentMonthField($month);
        $sql1 = "SELECT xe_id, $month_field as value FROM xe_currency WHERE currency_type = ? and default_currency = ? and change_currency = ? and currency_year = ?";
        if ($stmt1 = $this->conn->prepare($sql1)) {
            $stmt1->bind_param('ssss', $trendcurrency_type, $fromCur, $toCur, $year);
            $stmt1->execute();
            $stmt1->bind_result($xe_id, $value);
            $stmt1->store_result();
            $rowCount = $stmt1->num_rows;

            // echo $fromCur;
            // echo $toCur;
            // echo  MONTHLY_CURRENCY_URL;
            // echo '/?from=' . $fromCur . '&to=' . $toCur . '&month=' . $month . '&year=' . $byear . '&amount=1';
            // $data = file_get_contents(MONTHLY_CURRENCY_URL . '/?from=' . $fromCur . '&to=' . $toCur . '&month=' . $month . '&year=' . $byear . '&amount=1', false, $context);
            //         $data = json_decode($data, true);

            //         echo "<pre>";print_r($data);

            //         $currency = $data['to'][$fromCur][0]['monthlyAverage'];
            //         echo "<pre>";print_r($currency);
            // exit;

            if ($rowCount > 0) {
                $result = $stmt1->fetch();
                if ($value != 0.00) {
                    $trendavgCurrencyArray = array("month" => $month, "year" => $year, 'avgCur' => $value);
                } else {
                    $data = file_get_contents(MONTHLY_CURRENCY_URL . '/?from=' . $fromCur . '&to=' . $toCur . '&month=' . $month . '&year=' . $byear . '&amount=1', false, $context);
                    $data = json_decode($data, true);
                    $currency = $data['to'][$toCur][0]['monthlyAverage'];
                   // $currency = 2;
                    $this->UpdateCurrency($trendcurrency_type, $fromCur, $toCur, $month_field, $year, $currency, $xe_id);
                    $trendavgCurrencyArray = array("month" => $month, "year" => $year, 'avgCur' => $currency);
                }
            } else {
                $data = file_get_contents(MONTHLY_CURRENCY_URL . '/?from=' . $fromCur . '&to=' . $toCur . '&month=' . $month . '&year=' . $byear . '&amount=1', false, $context);
                $data = json_decode($data, true);
                $currency = $data['to'][$toCur][0]['monthlyAverage'];
                //$currency = 2;
                $this->storeCurrency($trendcurrency_type, $fromCur, $toCur, $month_field, $year, $currency);
                $trendavgCurrencyArray = array("month" => $month, "year" => $year, 'avgCur' => $currency);
            }
        }
        return $trendavgCurrencyArray;
    }

    public function storeCurrency($trendcurrency_type, $fromCur, $toCur, $month_field, $year, $currency)
    {
        $sql = 'INSERT INTO xe_currency (currency_type, default_currency, change_currency, currency_year, ' . $month_field . ') VALUES (?,?,?,?, ?)';
        if ($stmt1 = $this->conn->prepare($sql)) {
            $stmt1->bind_param("sssss", $trendcurrency_type, $fromCur, $toCur, $year, $currency);
            $stmt1->execute();
            $stmt1->store_result();
        }
    }

    public function UpdateCurrency($trendcurrency_type, $fromCur, $toCur, $month_field, $year, $currency, $xe_id)
    {
        $update_sql = "UPDATE xe_currency SET $month_field = '$currency'  WHERE xe_id = $xe_id";
        if ($stmt3 = $this->conn->prepare($update_sql)) {
            $stmt3->execute();
            $stmt3->store_result();
        }
    }

    public function checkYearOfBlncSheet($blnccurrency_type, $fromCur, $toCur, $month, $year, $lastdate, $monthDate)
    {
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode(ACCOUNT_ID . ':' . ACCOUNT_API_KEY),
            ),
        ));

        $utility = new utility();
        $month_field = $utility->getCurrentMonthField($month);
        $sql1 = "SELECT xe_id, $month_field as value FROM xe_currency WHERE currency_type = ? and default_currency = ? and change_currency = ? and currency_year = ?";
        if ($stmt1 = $this->conn->prepare($sql1)) {
            $stmt1->bind_param('ssss', $blnccurrency_type, $fromCur, $toCur, $year);
            $stmt1->execute();
            $stmt1->bind_result($xe_id, $value);
            $stmt1->store_result();
            $rowCount = $stmt1->num_rows;

            if ($rowCount > 0) {
                $result = $stmt1->fetch();
                if ($value != 0.00) {
                    $blncavgCurrencyArray = array("lastdate" => date('d M Y', strtotime($monthDate)), 'avgCur' => $value);
                } else {
                    $data = file_get_contents(HISTORIC_CURRENCY_URL . '/?from=' . $fromCur . '&to=' . $toCur . '&date=' . $lastdate . '&amount=1', false, $context);
                    $data = json_decode($data, true);
                    $currency = $data['to'][0]['mid'];
                   // $currency = 1;
                    $this->UpdateBlncCurrency($blnccurrency_type, $fromCur, $toCur, $month_field, $year, $currency, $xe_id);
                    $blncavgCurrencyArray = array("lastdate" => date('d M Y', strtotime($monthDate)), 'avgCur' => $currency);
                }
            } else {
                $data = file_get_contents(HISTORIC_CURRENCY_URL . '/?from=' . $fromCur . '&to=' . $toCur . '&date=' . $lastdate . '&amount=1', false, $context);
                $data = json_decode($data, true);
                $currency = $data['to'][0]['mid'];
               // $currency = 1;
                $this->storeBlncCurrency($blnccurrency_type, $fromCur, $toCur, $month_field, $year, $currency);
                $blncavgCurrencyArray = array("lastdate" => date('d M Y', strtotime($monthDate)), 'avgCur' => $currency);
            }
        }
        return $blncavgCurrencyArray;
    }

    public function storeBlncCurrency($blnccurrency_type, $fromCur, $toCur, $month_field, $year, $currency)
    {
        $sql = 'INSERT INTO xe_currency (currency_type, default_currency, change_currency, currency_year, ' . $month_field . ') VALUES (?,?,?,?, ?)';
        if ($stmt1 = $this->conn->prepare($sql)) {
            $stmt1->bind_param("sssss", $blnccurrency_type, $fromCur, $toCur, $year, $currency);
            $stmt1->execute();
            $stmt1->store_result();
        }
    }

    public function UpdateBlncCurrency($blnccurrency_type, $fromCur, $toCur, $month_field, $year, $currency, $xe_id)
    {
        $dash_db = new dashboardReport();
        $update_sql = "UPDATE xe_currency SET $month_field = '$currency'  WHERE xe_id = $xe_id";
        if ($stmt3 = $this->conn->prepare($update_sql)) {
            $stmt3->execute();
            $stmt3->store_result();
        }
    }

    public function storeJsonData($jsondata, $client_master_id)
    {
        $dash_db = new dashboardReport();
        $this->deleteMasterClientData($client_master_id);
        /* balancesheet */
        $balncData = $jsondata[1]['balanceSheet'];
        foreach ($balncData as $first_key => $first_array) {
            if ($first_key != "Total Assets" && $first_key != "Total Liabilities" && $first_key != "Net Assets") {
                foreach ($first_array as $second_key => $second_array) {
                    if($second_key != "no_expand"){
                        if(!is_numeric($second_key)){
                            if ($second_key != "summaryRow") {
                                $code = 0;
                                $category_flag = 2;
                                switch ($first_key) {
                                    case "Bank":
                                        $type = "BANK";
                                        $order = 1;
                                        break;
                                    case "Current Assets":
                                        $type = "CURRENT";
                                        $order = 2;
                                        break;
                                    case "Fixed Assets":
                                        $type = "FIXED";
                                        $order = 3;
                                        break;
                                    case "Current Liabilities":
                                        $type = "CURRLIAB";
                                        $order = 4;
                                        break;
                                    case "Liability":
                                        $type = "LIABILITY";
                                        $order = 5;
                                        break;
                                    case "Non-Current Liabilities":
                                        $type = "TERMLIAB";
                                        $order = 6;
                                        break;
                                    case "Equity":
                                        $type = "EQUITY";
                                        $order = 7;
                                        break;
                                    case "Prepayment":
                                        $type = "PREPAYMENT";
                                        $order = 8;
                                        break;
                                    case "PAYG Liability":
                                        $type = "PAYGLIABILITY";
                                        $order = 9;
                                        break;
                                    case "Inventory Asset":
                                        $type = "INVENTORY";
                                        $order = 10;
                                        break;
                                    case "Non-Current Assets":
                                        $type = "NONCURRENT";
                                        $order = 11;
                                        break;
                                    case "Overhead":
                                        $type = "OVERHEADS";
        
                                        $order = 12;
                                        break;
                                    case "Superannuation Expense":
                                        $type = "SUPERANNUATIONEXPENSE";
                                        $order = 13;
                                        break;
                                    case "Superannuation Liability":
                                        $type = "SUPERANNUATIONLIABILITY";
                                        $order = 14;
                                        break;
                                    case "Wages Expense":
                                        $type = "WAGESEXPENSE";
                                        $order = 15;
                                        break;
                                }
                                $this->insertAccountCategories($client_master_id, $code, $second_key, $type, $category_flag, $first_key, $order);
                            }
                        }
                    }
                }
            }
        }

        /* group campanies account categories */
        $trendData = $jsondata[3]['trendPNL'];
        foreach ($trendData as $first_trendKey => $firstTrend_array) {
            if ($first_trendKey != "Gross Profit" && $first_trendKey != "Operating Profit" && $first_trendKey != "Net Profit" && $first_trendKey != "Operating Expenses" && $first_trendKey != "trendPNL_Quartely") {
                foreach ($firstTrend_array as $sec_trendKey => $sec_Trendarray) {
                    if ($sec_trendKey != "summaryRow" && $sec_trendKey != "Employee Compensation & Related Expenses") {
                        $code = 0;
                        $category_flag = 1;

                        switch ($first_trendKey) {
                            case "Income":
                                $type = "REVENUE";
                                $order = 1;
                                break;
                            case "Less Cost of Sales":
                                $type = "DIRECTCOSTS";
                                $order = 2;
                                break;
                            case "Less Operating Expenses":
                                $type = "EXPENSE";
                                $order = 3;
                                break;
                            case "Non-operating Income":
                                $type = "OTHERINCOME";
                                $order = 5;
                                break;
                            case "Non-operating Expenses":
                                $type = "DEPRECIATNs";
                                $order = 6;
                                break;
                        }

                        $this->insertAccountCategories($client_master_id, $code, $sec_trendKey, $type, $category_flag, $first_trendKey, $order);

                    } else if ($sec_trendKey != "summaryRow") {
                        foreach ($sec_Trendarray as $third_trendKey => $third_trendArray) {
                            if ($third_trendKey != "summaryRow") {
                                switch ($sec_trendKey) {
                                    case "Employee Compensation & Related Expenses":
                                        $type = "EMPCOMP";
                                        $order = 4;
                                }
                                $this->insertAccountCategories($client_master_id, $code, $third_trendKey, $type, $category_flag, $sec_trendKey, $order);
                            }

                        }
                    }
                }
            }
        }
    }

    public function updateEliminateByName($account_name)
    {
        $response = array();
        try
        {
            $sql = "UPDATE group_companies_account_categories SET is_eliminated = 1  WHERE account_name = ? and client_master_id = ? and group_id = ?";
            if ($stmt = $this->conn->prepare($sql)) {
                foreach ($account_name as $value) {
                    foreach ($value as $v1) {
                        foreach ($v1 as $v2) {
                            $masterId = $v2['client_master_id'];
                            $groupID = $v2['group_id'];
                            $account_name = $v2['account_name'];
                            $stmt->bind_param("sii", $account_name, $masterId, $groupID);
                            $result = $stmt->execute();
                            $this->conn->commit();
                            $response["error"] = false;
                            $response["message"] = Eliminate_UPDATE_SUCCESS;

                        }
                    }
                }
            } else {
                $response["error"] = false;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function deleteMasterClientData($client_master_id)
    {
        try {
            $response = array();
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            $childSQL = 'DELETE FROM master_companies_account_categories WHERE client_master_id = ?';
            if ($childStmt = $this->conn->prepare($childSQL)) {
                $childStmt->bind_param('i', $client_master_id);
                $result = $childStmt->execute();
                $childStmt->close();
                if ($result) {
                    $this->conn->commit();
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }

            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function insertAccountCategories($client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $order)
    {
        try {
            $this->conn->autocommit(false);
            // $account_category_record = $this->getAccountDataCheck($client_master_id, $account_code);
            $account_category_record["error"] = true;
            if ($account_category_record["error"] == true) {
                $sql = "INSERT INTO  master_companies_account_categories  (client_master_id, account_code, account_name, account_type, account_category, account_type_alias, account_order) VALUES (?,?,?,?,?,?,?)";
                $e = 1;
            } else {
                $id = $account_category_record['field_key'];
                $sql = "UPDATE  master_companies_account_categories set account_name = ?, account_type = ?, account_category = ?, account_type_alias = ?, account_order = ? where client_master_id = ?  and id = ? ";
                $e = 2;
            }

            if ($stmt = $this->conn->prepare($sql)) {
                if ($e == 1) {
                    $stmt->bind_param("isssiss", $client_master_id, $account_code, $account_name, $type, $category_flag, $account_type_rename, $order);
                } else {
                    $stmt->bind_param("ssisiii", $account_name, $type, $category_flag, $account_type_rename, $order, $client_master_id, $account_category_record['field_key']);
                }
                $result = $stmt->execute();
                $stmt->close();
                if ($result) {
                    $this->conn->commit();
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getConsolidationReportData($aPNLTrend, $report_start_date, $budgetSummarys, $consolidate_month_arr, $endDateResult, $endDate = null)
    {
        $client = new client();
        $TrendJfoData = collect($aPNLTrend);
        if ($endDateResult == 1) {
            $TrendData = $TrendJfoData->collapse()->toArray();
        } else {
            $TrendData['trendPNL'] = $TrendJfoData->collapse()->toArray();
        }

        $trendPNL = new TrendPNL($TrendData);
        /* operating expenses */
        $top_exp = $trendPNL->LessOperatingExpenses();
        $emp_exp = $trendPNL->LessOperatingExpensesAndEmp();

        $finalData = $this->ArraySearch($top_exp, "id", $report_start_date);
        $finalData = $this->ArrayFilter($finalData);
        $finalData = $this->ArraySearchss($finalData);
        /* array filter for emplyee compensation data */
        $empfinalData = $this->ArraySearch($emp_exp, "id", $report_start_date);
        $empfinalData = $this->ArrayFilter($empfinalData);
        $empfinalData = $this->ArraySearchss($empfinalData);
        /* final operating expenses sorting array data*/
        $finalData['Total Employee Compensation & Related Expenses'] = $empfinalData['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'];
        unset($finalData['Employee Compensation & Related Expenses']);
        unset($finalData['summaryRow']);
        arsort($finalData);

        if ($endDateResult == 0) {
            $curdate = date('Y-m-t');
            $consData = $this->trendpnlarray($trendPNL, $finalData);
            /* MTD values */
            $finalConsDataMTD = $this->ConsMTDArraySearch($consData, "id", $curdate);
            $mtd = 1;
            $consolidateMTD = $this->consolidatePNL($finalConsDataMTD, $mtd, $budgetSummarys, $consolidate_month_arr);
            $totalbudgetCalMTD = $this->totalbudgetCal($consolidateMTD);
            /* YTD values */
            $finalConsData = $this->ConsolidateArraySearch($consData, "id", $report_start_date);
            $mtd = 2;
            $consolidatePNLYTD = $this->consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr);
            $totalbudgetCalYTD = $this->totalbudgetCal($consolidatePNLYTD);
            /* YTY values */
            $mtd = 0;
            $consolidatePNLYTY = $this->consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr);
            $totalbudgetCalYTY = $this->totalbudgetCal($consolidatePNLYTY);
            /* YTD_MTD values */
            $consolidatePNLYTDandMTD = $client->ArrayPush($totalbudgetCalMTD, $totalbudgetCalYTD);
            $array['consolidatedPNL_mtd']['consolidatedPNL_mtd'] = $totalbudgetCalMTD;
            $array['consolidatedPNL_ytd']['consolidatedPNL_ytd'] = $totalbudgetCalYTD;
            $array['consolidatedPNL_yty']['consolidatedPNL_yty'] = $totalbudgetCalYTY;
            $array['consolidatedPNL_ytd_mtd']['consolidatedPNL_ytd_mtd'] = $consolidatePNLYTDandMTD;
        } else {
            $consData = $this->trendpnlarray($trendPNL, $finalData);
            /* MTD values */
            $finalConsDataMTD = $this->ConsMTDArraySearch($consData, "id", $endDate);
            $mtd = 1;
            $consolidateMTD = $this->consolidatePNL($finalConsDataMTD, $mtd, $budgetSummarys, $consolidate_month_arr);
            $totalbudgetCalMTD = $this->totalbudgetCal($consolidateMTD);
            /* YTD values */
            $finalConsData = $this->ConsEndDateArraySearch($consData, "id", $report_start_date, $endDate);
            $mtd = 2;
            $consolidatePNLYTDByEndDate = $this->consolidatePNL($finalConsData, $mtd, $budgetSummarys, $consolidate_month_arr);
            $totalbudgetCalYTD = $this->totalbudgetCal($consolidatePNLYTDByEndDate);
            /* YTD_MTD values */
            $consolidatePNLYTDandMTD = $client->ArrayPush($totalbudgetCalMTD, $totalbudgetCalYTD);
            $array['consolidatedPNL'] = $consolidatePNLYTDandMTD;
        }
        return $array;
    }

    public function totalbudgetCal($consolidateMTD){
        /* income section */
        $incomeData = $consolidateMTD['Income'];
        if(!empty($incomeData)){
            $incomeAmt = 0;
            foreach($incomeData as $incomeKey => $incomeVal){
                $incomeAmt += $incomeVal[2]['Value'];
            }
            $consolidateMTD['Income']['summaryRow']['Total Income'][2]['Value'] = $incomeAmt;
            $consolidateMTD['Income']['summaryRow']['Total Income'][3]['Value'] = round($consolidateMTD['Income']['summaryRow']['Total Income'][1]['Value'] - $incomeAmt);
            $consolidateMTD['Income']['summaryRow']['Total Income'][4]['Value'] = $this->summaryVarPerCalulation($incomeAmt, $consolidateMTD['Income']['summaryRow']['Total Income'][3]['Value'] );
        }
        /* Less Cost of Sales section */
        $COSAmt = 0;
        $cosData = $consolidateMTD['Less Cost of Sales'];
        if(!empty($cosData)){
            foreach($cosData as $COSKey => $COSVal){
                $COSAmt += $COSVal[2]['Value'];
            }
            $consolidateMTD['Less Cost of Sales']['summaryRow']['Total Cost of Sales'][2]['Value'] = $COSAmt;
            $consolidateMTD['Less Cost of Sales']['summaryRow']['Total Cost of Sales'][3]['Value'] = round($consolidateMTD['Less Cost of Sales']['summaryRow']['Total Cost of Sales'][1]['Value'] - $COSAmt);
            $consolidateMTD['Less Cost of Sales']['summaryRow']['Total Cost of Sales'][4]['Value'] = $this->summaryVarPerCalulation($COSAmt, $consolidateMTD['Less Cost of Sales']['summaryRow']['Total Cost of Sales'][3]['Value'] );
            /* gross profit section */
            $grossProfitAmt = $incomeAmt - $COSAmt;
            $consolidateMTD['Gross Profit']['Gross Profit'][2]['Value'] = $grossProfitAmt;
            $consolidateMTD['Gross Profit']['Gross Profit'][3]['Value'] = round($consolidateMTD['Gross Profit']['Gross Profit'][1]['Value'] - $grossProfitAmt);
            $consolidateMTD['Gross Profit']['Gross Profit'][4]['Value'] = $this->summaryVarPerCalulation($grossProfitAmt, $consolidateMTD['Gross Profit']['Gross Profit'][3]['Value'] );
        }
        /* Less Operating Expenses section */
        $LOEAmt = 0;
        $LOEData = $consolidateMTD['Less Operating Expenses'];
        foreach($LOEData as $LOEKey => $LOEVal){
            $LOEAmt += $LOEVal[2]['Value']; 
        }
        /* Employee Compensation & Related Expenses */
        $EMPAmt = 0;
        $EMPData = $consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses'];
        foreach($EMPData as $EMPKey => $EMPVal){
            $EMPAmt += $EMPVal[2]['Value']; 
        }
        $consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'][2]['Value'] = $EMPAmt;
        $consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'][3]['Value'] = round(abs($consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'][1]['Value']) - abs($LOEAmt));
        $consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'][4]['Value'] = $this->summaryVarPerCalulation($LOEAmt, $consolidateMTD['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']['Total Employee Compensation & Related Expenses'][3]['Value'] );
        /* Total Operating Expenses */
        $totalOPAmt = $LOEAmt + $EMPAmt;
        $consolidateMTD['Less Operating Expenses']['summaryRow']['Total Operating Expenses'][2]['Value'] = $totalOPAmt;
        $consolidateMTD['Less Operating Expenses']['summaryRow']['Total Operating Expenses'][3]['Value'] = round($consolidateMTD['Less Operating Expenses']['summaryRow']['Total Operating Expenses'][1]['Value'] - $totalOPAmt);
        $consolidateMTD['Less Operating Expenses']['summaryRow']['Total Operating Expenses'][4]['Value'] = $this->summaryVarPerCalulation($totalOPAmt, $consolidateMTD['Less Operating Expenses']['summaryRow']['Total Operating Expenses'][3]['Value'] );
        /* Operating Profit section */
        $OPAmt = $grossProfitAmt - $totalOPAmt;
        $consolidateMTD['Operating Profit']['Operating Profit'][2]['Value'] = $OPAmt;
        $consolidateMTD['Operating Profit']['Operating Profit'][3]['Value'] = round($consolidateMTD['Operating Profit']['Operating Profit'][1]['Value'] - $OPAmt);
        $consolidateMTD['Operating Profit']['Operating Profit'][4]['Value'] = $this->summaryVarPerCalulation($OPAmt, $consolidateMTD['Operating Profit']['Operating Profit'][3]['Value'] );
        /* Non-operating Income section */
        $nonOpAmt = 0;
        $nonOpIncomeData = $consolidateMTD['Non-operating Income'];
        if(!empty($nonOpIncomeData)){
            foreach($nonOpIncomeData as $key => $nonOpIncomeVal){
                $nonOpAmt += $nonOpIncomeVal[2]['Value'];
            }
            $consolidateMTD['Non-operating Income']['summaryRow']['Total Non-operating Income'][2]['Value'] = $nonOpAmt;
            $consolidateMTD['Non-operating Income']['summaryRow']['Total Non-operating Income'][3]['Value'] = round($consolidateMTD['Non-operating Income']['summaryRow']['Total Non-operating Income'][1]['Value'] - $nonOpAmt);
            $consolidateMTD['Non-operating Income']['summaryRow']['Total Non-operating Income'][4]['Value'] = $this->summaryVarPerCalulation($nonOpAmt, $consolidateMTD['Non-operating Income']['summaryRow']['Total Non-operating Income'][3]['Value'] );
        }
        /* Non-operating Expenses section */
        $nonOPExAmt = 0;
        $nonOpExData = $consolidateMTD['Non-operating Expenses'];
        foreach($nonOpExData as $key => $nonOpExpVal){
            $nonOPExAmt += $nonOpExpVal[2]['Value'];
        }
        $consolidateMTD['Non-operating Expenses']['summaryRow']['Total Non-operating Expenses'][2]['Value'] = $nonOPExAmt;
        $consolidateMTD['Non-operating Expenses']['summaryRow']['Total Non-operating Expenses'][3]['Value'] = round($consolidateMTD['Non-operating Expenses']['summaryRow']['Total Non-operating Expenses'][1]['Value'] - $nonOPExAmt);
        $consolidateMTD['Non-operating Expenses']['summaryRow']['Total Non-operating Expenses'][4]['Value'] = $this->summaryVarPerCalulation($nonOPExAmt, $consolidateMTD['Non-operating Expenses']['summaryRow']['Total Non-operating Expenses'][3]['Value'] );
        $netProfitIncome = $OPAmt + $nonOpAmt - $nonOPExAmt;
        /* Net Profit section */
        $consolidateMTD['Net Profit']['Net Profit'][2]['Value'] = $netProfitIncome;
        $consolidateMTD['Net Profit']['Net Profit'][3]['Value'] = round($consolidateMTD['Net Profit']['Net Profit'][1]['Value'] - $netProfitIncome);
        $consolidateMTD['Net Profit']['Net Profit'][4]['Value'] = $this->summaryVarPerCalulation($netProfitIncome, $consolidateMTD['Net Profit']['Net Profit'][3]['Value'] );

        return $consolidateMTD;

    }

    public function summaryVarPerCalulation($budget, $variance){
        $variance_Percentage = 0;
        if ($budget != 0 || $budget != '0.0') {
            $variance_Percentage = round(($variance / abs($budget)) * 100);
        } 
        return $variance_Percentage;
    }

    public function pushTotalByEmptyArray($finalConsData, $data){
        foreach($finalConsData['Income'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Income'][$key]['total'] = 0;
            }else{
                $finalConsData['Income'][$key] = $value;
            }
        }
        if(isset($data['Income'])){
            $finalConsData['Income'] = array_merge($finalConsData['Income'],  $data['Income']);
            $expSummarData = $finalConsData['Income']['summaryRow'];
            unset($finalConsData['Income']['summaryRow']);
            $finalConsData['Income']['summaryRow'] = $expSummarData;
        }

        foreach($finalConsData['Less Cost of Sales'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Less Cost of Sales'][$key]['total'] = 0;
            }else{
                $finalConsData['Less Cost of Sales'][$key] = $value;
            }
        }

        if(isset($data['Less Cost of Sales'])){
            $finalConsData['Less Cost of Sales'] = array_merge($finalConsData['Less Cost of Sales'],  $data['Less Cost of Sales']);
            $expSummarData = $finalConsData['Less Cost of Sales']['summaryRow'];
            unset($finalConsData['Less Cost of Sales']['summaryRow']);
            $finalConsData['Less Cost of Sales']['summaryRow'] = $expSummarData;
        }
        foreach($finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'][$key]['total'] = 0;
            }else{
                $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'][$key] = $value;
            }
        }

        if(isset($data['Less Operating Expenses'])){
            $expenseVal = $data['Less Operating Expenses'];
        }else{
            $expenseVal = $data['Operating Expenses'];
        }

        if(isset($expenseVal['Employee Compensation & Related Expenses'])){
            $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'] = array_merge($finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'],  $expenseVal['Employee Compensation & Related Expenses']);
            $expSummarData = $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow'];
            unset($finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow']);
            $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses']['summaryRow'] = $expSummarData;
        }
        foreach($finalConsData['Less Operating Expenses'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Less Operating Expenses'][$key]['total'] = 0;
            }else{
                $finalConsData['Less Operating Expenses'][$key] = $value;
            }
        }

        if(isset($expenseVal)){
            unset($expenseVal['Employee Compensation & Related Expenses']);
            $employeeComp = $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'];
            unset($finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses']);
            $finalConsData['Less Operating Expenses'] = array_merge($finalConsData['Less Operating Expenses'], $expenseVal);
            ksort($finalConsData['Less Operating Expenses']);
            $summaryData = $finalConsData['Less Operating Expenses']['summaryRow'];
            unset($finalConsData['Less Operating Expenses']['summaryRow']);
            $finalConsData['Less Operating Expenses']['Employee Compensation & Related Expenses'] = $employeeComp;
            $finalConsData['Less Operating Expenses']['summaryRow'] = $summaryData;
        }
       
        foreach($finalConsData['Non-operating Income'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Non-operating Income'][$key]['total'] = 0;
            }else{
                $finalConsData['Non-operating Income'][$key] = $value;
            }
        }
        if(isset($data['Non-operating Income'])){
            $finalConsData['Non-operating Income'] = array_merge($finalConsData['Non-operating Income'],  $data['Non-operating Income']);
            $expSummarData = $finalConsData['Non-operating Income']['summaryRow'];
            unset($finalConsData['Non-operating Income']['summaryRow']);
            $finalConsData['Non-operating Income']['summaryRow'] = $expSummarData;
        }

        foreach($finalConsData['Non-operating Expenses'] as $key => $value){
            if(sizeof($value) == 0){
                $finalConsData['Non-operating Expenses'][$key]['total'] = 0;
            }else{
                $finalConsData['Non-operating Expenses'][$key] = $value;
            }
        }

        if(isset($data['Non-operating Expenses'])){
            $finalConsData['Non-operating Expenses'] = array_merge($finalConsData['Non-operating Expenses'],  $data['Non-operating Expenses']);
            $expSummarData = $finalConsData['Non-operating Expenses']['summaryRow'];
            unset($finalConsData['Non-operating Expenses']['summaryRow']);
            $finalConsData['Non-operating Expenses']['summaryRow'] = $expSummarData;
        }
        return $finalConsData;
    }
}
