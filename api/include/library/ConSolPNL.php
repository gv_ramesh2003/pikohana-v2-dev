<?php
/**
 *
 */
class ConSolPNL
{
    private $jsondata;
    private $keyname;
    public function __construct(array $jsondata, $keyname)
    {
        $this->jsondata = $jsondata;
        $this->keyname = $keyname;
    }


    public function get($childname)
    {
        $jsondata = $this->jsondata[$this->keyname][$this->keyname][$childname];
        foreach ($jsondata as $key => $value) {
            if ($key!=="summaryRow" && $key!=="Employee Compensation & Related Expenses") {
                if (is_array($value)) {
                    $jsondata[$key][2] =  ["Value"=>0.0];
                    $jsondata[$key][4] =  ["Value"=>0.0];
                    $jsondata[$key][3] =  $jsondata[$key][1];
                }
            } 
            elseif ($key==="summaryRow") {
                foreach ($jsondata['summaryRow'] as $jkey => $jvalue) {
                    if (is_array($jvalue)) {
                        $jsondata[$key][$jkey][2] =  ["Value"=>0.0];
                        $jsondata[$key][$jkey][4] = ["Value"=>0.0];
                        if (isset($jsondata[$key][$jkey][1])) {
                            $jsondata[$key][$jkey][3] =$jsondata[$key][$jkey][1];
                        } else {
                            $jsondata[$key][$jkey][3] =["Value"=>0.0];
                        }
                    }
                }
            } else {
                foreach ($jsondata['Employee Compensation & Related Expenses'] as $ekey => $evalue) {
                    if ($ekey==="summaryRow") {
                        foreach ($jsondata['Employee Compensation & Related Expenses'][$ekey] as $kkey => $kvalue) {
                            if (is_array($kvalue)) {
                                $jsondata[$key][$ekey][$kkey][2] =  ["Value"=>0.0];
                                $jsondata[$key][$ekey][$kkey][4] = ["Value"=>0.0];
                                if (isset($jsondata[$key][$ekey][$kkey][1])) {
                                    $jsondata[$key][$ekey][$kkey][3] =$jsondata[$key][$ekey][$kkey][1];
                                } else {
                                    $jsondata[$key][$ekey][$kkey][3] =["Value"=>0.0];
                                }
                            }
                        }
                    }
                }
            }
        }
        return $jsondata;
    }
}
