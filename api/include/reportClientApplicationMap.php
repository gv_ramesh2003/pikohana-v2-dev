<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
require_once dirname(__FILE__) . '/DbConnect.php';
class reportClientApplicationMap extends utility
{

    // private $conn;

    public function __construct()
    {

        // opening db connection
        $this->db = new DbConnect();
        $this->conn = $this->db->connect();
    }

    public function checkIfReportClientApplicationMapExist($report_id, $client_application_map_id)
    {
        try {
            $sql = "SELECT report_client_app_map_id, is_active FROM report_client_application_map WHERE report_id = ? AND client_application_map_id = ? ";
            if ($stmt = $this->conn->prepare($sql)) {
                $stmt->bind_param("ii", $report_id, $client_application_map_id);
                $stmt->execute();
                $stmt->store_result();
                //$stmt->close();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($report_client_app_map_id, $is_active);
                    while ($result = $stmt->fetch()) {
                        $response["error"] = false;
                        $response["message"] = RECORD_FOUND;
                        $response["is_active"] = $is_active;
                        $response['report_client_app_map_id'] = $report_client_app_map_id;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }

            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function addNewReportClientApplicationMap($report_id, $client_id, $application_id, $client_master_id)
    {
        try {
            $checkResult = array();
            $resp = array();
            $objClientApp = new clientApplicationMap();
            $resp = $objClientApp->checkIfClientApplicationMapExist($client_id, $client_master_id, $application_id);
            if (sizeof($resp) > 0) {
                if ($resp['error'] == false) {
                    $client_application_map_id = $resp['client_application_map_id'];
                    $checkResult = $this->checkIfReportClientApplicationMapExist($report_id, $client_application_map_id);
                    if ($checkResult['error'] == true) {
                        $this->conn->autocommit(false);
                        $date = date("Y-m-d h:i:s");
                        $is_active = 1;
                        $response = array();
                        if ($stmt = $this->conn->prepare('INSERT INTO report_client_application_map(report_id, client_application_map_id, created_date, last_updated_on, is_active) values(?, ?, ?, ?, ?)')) {
                            $stmt->bind_param('iissi', $report_id, $client_application_map_id, $date, $date, $is_active);
                            $result = $stmt->execute();
                            $stmt->close();
                            if ($result) {
                                $report_client_app_map_id = $this->conn->insert_id;
                                $this->conn->commit();
                                $response["error"] = false;
                                $response["message"] = REPORT_CLIENT_APPLICATION_MAP_CREATED_SUCCESS;
                                $response['report_client_app_map_id'] = $report_client_app_map_id;
                            } else {
                                $response["error"] = true;
                                $response["message"] = REPORT_CLIENT_APPLICATION_MAP_CREATED_FAILURE;
                            }
                        } else {
                            $response["error"] = true;
                            $response["message"] = QUERY_EXCEPTION;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = ALREADY_EXIST;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = NO_RECORD_FOUND;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function addNewReportClientApplicationMap1($report_id, $client_id, $application_id, $client_application_map_id)
    {
        try {
            $checkResult = array();
            $resp = array();
            $checkResult = $this->checkIfReportClientApplicationMapExist($report_id, $client_application_map_id);
            if ($checkResult['error'] == true) {
                $this->conn->autocommit(false);
                $date = date("Y-m-d h:i:s");
                $is_active = 1;
                $response = array();
                if ($stmt = $this->conn->prepare('INSERT INTO report_client_application_map(report_id, client_application_map_id, created_date, last_updated_on, is_active) values(?, ?, ?, ?, ?)')) {
                    $stmt->bind_param('iissi', $report_id, $client_application_map_id, $date, $date, $is_active);
                    $result = $stmt->execute();

                    $stmt->close();
                    if ($result) {
                        $report_client_app_map_id = $this->conn->insert_id;
                        $this->conn->commit();
                        $response["error"] = false;
                        $response["message"] = REPORT_CLIENT_APPLICATION_MAP_CREATED_SUCCESS;
                        $response['report_client_app_map_id'] = $report_client_app_map_id;
                    } else {
                        $response["error"] = true;
                        $response["message"] = REPORT_CLIENT_APPLICATION_MAP_CREATED_FAILURE;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = QUERY_EXCEPTION;
                }
            } else {
                $response["error"] = true;
                $response["message"] = ALREADY_EXIST;
            }

            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getAllReportClientApplicationMaps($searchItems)
    {
        try {
            $sql = "SELECT rcam.report_client_app_map_id, rm.report_id, rcam.client_application_map_id, rm.report_name, rm.report_auth_key,
							rm.is_source_single, cam.client_id, cam.application_id, cam.auth_key, am.application_name, cm.name, cm.email, cm.mobile,
							cmd.company_name, am.application_logo_path, am.application_api_url, am.application_type, rcam.created_date, rcam.is_active,
							rcam.last_updated_on, am.is_system_token_required, am.is_user_auth_key_required, report_type, am.app_site_url,
							report_logo_path, is_admin,cam.is_active as application_is_active
					 FROM report_client_application_map rcam
					 RIGHT JOIN client_application_map cam ON cam.client_application_map_id = rcam.client_application_map_id
					 RIGHT JOIN report_master rm ON rm.report_id = rcam.report_id
					 RIGHT JOIN application_master am ON am.application_id = cam.application_id
					 RIGHT JOIN client_master cm ON cm.client_id = cam.client_id
					 RIGHT JOIN client_master_detail cmd ON cmd.client_master_id = cam.client_master_id
					WHERE  ";

            $response["reportClientApplicationMapDetails"] = array();
            if (sizeof($searchItems) <= 0) {
                $searchItems['is_active'] = 1;
                $searchItems['is_active1'] = 1;
            }
            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cam.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'cam.client_master_id = ? ';
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cam.is_active = ? ";
                        break;
                    case 'is_active1':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "rcam.is_active = ? ";
                        break;
                    case 'report_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "rcam.report_id = ? ";
                        break;
                    case 'application_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = " cam.application_id = ? ";
                        break;
                    case 'auth_key':
                        $a_param_type[] = 's';
                        $a_bind_params[] = $value;
                        $query[] = " trim(cam.auth_key) != ? ";
                        break;
                    default:
                        break;
                }
            }
            $sql .= implode(' AND ', $query);

            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $goAhead = true;
                    $aReportSource = array();
                    $stmt->bind_result($report_client_app_map_id, $report_id, $client_application_map_id, $report_name, $report_auth_key, $is_source_single, $client_id, $application_id, $auth_key, $application_name, $name, $email, $phone, $company_name, $application_logo_path, $application_api_url, $application_type, $created_date, $is_active, $last_updated_on, $is_system_token_required, $is_user_auth_key_required, $report_type, $app_site_url, $report_logo_path, $is_admin, $application_is_active);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["report_client_app_map_id"] = $report_client_app_map_id;
                        $tmp["report_id"] = $report_id;
                        $tmp["client_application_map_id"] = $client_application_map_id;
                        $tmp["report_name"] = $report_name;
                        $tmp["client_id"] = $client_id;
                        $tmp["application_id"] = $application_id;
                        $tmp["auth_key"] = $auth_key;
                        $tmp["report_auth_key"] = $report_auth_key;
                        $tmp["report_logo_path"] = $report_logo_path;
                        $tmp["is_source_single"] = $is_source_single;
                        $tmp["application_name"] = $application_name;
                        $tmp["name"] = $name;
                        $tmp["email"] = $email;
                        $tmp["phone"] = $phone;
                        $tmp["company_name"] = $company_name;
                        $tmp["application_logo_path"] = $application_logo_path;
                        $tmp["application_api_url"] = $application_api_url;
                        $tmp["app_site_url"] = $app_site_url;
                        $tmp["application_type"] = $application_type;
                        $tmp["created_date"] = $created_date;
                        $tmp["is_active"] = $is_active;
                        $tmp["is_system_token_required"] = $is_system_token_required;
                        $tmp["is_user_auth_key_required"] = $is_user_auth_key_required;
                        $tmp["report_type"] = $report_type;
                        $tmp["application_is_active"] = $application_is_active;
                        $aReportSource = explode(',', $report_type);
                        $tmp["report_type_as_array"] = $aReportSource;

                        $response["reportClientApplicationMapDetails"][] = $tmp;
                        $response['report_ids'][] = $report_id;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            $aReportSource = null;
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllReportClientApplicationMapWrapper($searchParams)
    {
        try {
            $response = array();
            $newResponse = array();
            $searchItems = array();
            $abc = array();
            $newResponse = $this->getAllReportClientApplicationMaps($searchParams);
            $actualReports = $newResponse["report_ids"];

            if (isset($searchParams["admin_active_reports"])) {
                return $newResponse;
            } else {
                if ($newResponse['error'] == false) {
                    $searchItems['more_reports'] = implode(',', $newResponse['report_ids']);
                } else {
                    $searchItems["is_active"] = 1;
                }
                if (isset($searchItems["master_reports"]) &&  sizeof($searchParams["master_reports"]) > 0) {
                    $searchItems["master_reports"] = $searchParams["master_reports"];
                }

                $objAppln = new report();
                $is_active = 1;
                $response = $objAppln->getAllReports($searchItems);
                if ($response['error'] == false) {
                    $newResponse["error"] = false;
                    foreach ($response['reportDetails'] as $k => $v) {
                        $v["report_client_app_map_id"] = null;
                        $v["client_application_map_id"] = null;
                        $newResponse['reportClientApplicationMapDetails'][] = $v;
                    }
                }
            }

            return $newResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function updateReportClientApplicationMap($report_client_app_map_id, $client_id, $application_id, $client_master_id)
    {
        try {

            $resp = array();
            $objClientApp = new clientApplicationMap();
            $resp = $objClientApp->checkIfClientApplicationMapExist($client_id, $client_master_id, $application_id);

            if (sizeof($resp) > 0) {
                if ($resp['error'] == false) {
                    $client_application_map_id = $resp['client_application_map_id'];
                    $response = array();
                    $date = date("Y-m-d h:i:s");
                    $this->conn->autocommit(false);
                    $is_active = 1;
                    if ($stmt = $this->conn->prepare("UPDATE report_client_application_map set client_application_map_id = ?, last_updated_on = ?  WHERE report_client_app_map_id = ? AND is_active = ?")) {
                        $stmt->bind_param("isii", $client_application_map_id, $date, $report_client_app_map_id, $is_active);
                        $result = $stmt->execute();
                        $this->conn->commit();
                        $stmt->close();
                        if ($result) {
                            $response["error"] = false;
                            $response["message"] = UPDATE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS;
                        } else {
                            $response["error"] = true;
                            $response["message"] = UPDATE_REPORT_CLIENT_APPLICATION_MAP_FAILURE;
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = QUERY_EXCEPTION;
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = NO_RECORD_FOUND;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    /**
     * Deleting a branch
     * @param String $industry_type_id id of the task to delete
     */
    public function deleteReportClientApplicationMap($report_client_app_map_id)
    {
        try {
            $response = array();
            $is_active = 0;
            $date = date("Y-m-d h:i:s");
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("DELETE FROM report_client_application_map WHERE report_client_app_map_id = ?")) {
                $stmt->bind_param("i", $report_client_app_map_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function deletedMappedReports($client_application_map_id)
    {
        try {
            $response = array();
            $is_active = 0;
            $date = date("Y-m-d h:i:s");
            $report_id = KPI;
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("DELETE FROM report_client_application_map WHERE client_application_map_id = ? AND report_id != ?")) {
                $stmt->bind_param("ii", $client_application_map_id, $report_id);
                $result = $stmt->execute();
                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function deletedAllMappedReports($ids_to_delete)
    {
        try {
            $response = array();
            $delete_ids = implode(',', $ids_to_delete);
            //    echo '<pre> delete repots'; print_r($delete_ids); echo '</pre>';
            $this->conn->autocommit(false);
            if ($stmt = $this->conn->prepare("DELETE FROM report_client_application_map WHERE client_application_map_id IN ($delete_ids)")) {
                //    $stmt->bind_param("i", $client_application_map_id);
                $result = $stmt->execute();

                $this->conn->commit();
                $stmt->close();
                if ($result) {
                    $response["error"] = false;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_SUCCESS;
                } else {
                    $response["error"] = true;
                    $response["message"] = DELETE_REPORT_CLIENT_APPLICATION_MAP_FAILURE;
                }
            } else {

                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            return $response;
        } catch (Exception $e) {
            $this->conn->rollback();
            echo $e->getMessage();
        }
    }

    public function getClientReportApplication($searchItems)
    {
        try {
            $sql1 = "SELECT cm.name, cmd.company_name, is_admin
                    FROM client_user_mapping cum
                    JOIN client_master cm ON cm.client_id = cum.client_id
                    JOIN client_master_detail cmd ON cmd.client_master_id = cum.client_master_id
                    WHERE ";

            $a_params = array();
            $a_param_type = array();
            $a_bind_params = array();
            $query = array();

            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cum.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'cum.client_master_id = ? ';
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cm.is_active = ?";
                        break;
                    case 'is_active1':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cmd.is_active = ?";
                        break;
                    default:
                        break;
                }
            }
            $sql1 .= implode(' AND ', $query);
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            if ($stmt = $this->conn->prepare($sql1)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $stmt->bind_result($name, $company_name, $is_admin);
                    while ($result = $stmt->fetch()) {
                        $response["name"] = $name;
                        $response["company_name"] = $company_name;
                        $response["is_admin"] = $is_admin;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                }
            }

            $response["reportClientApplicationMapDetails"][] = array('report_id' => 4);

            $sql = "SELECT rcam.report_client_app_map_id, rm.report_id, rcam.client_application_map_id, rm.report_name,
                                cam.client_id, cam.application_id,  am.application_name
                FROM report_client_application_map rcam
                RIGHT JOIN client_application_map cam ON cam.client_application_map_id = rcam.client_application_map_id
                RIGHT JOIN report_master rm ON rm.report_id = rcam.report_id
                RIGHT JOIN application_master am ON am.application_id = cam.application_id
                WHERE  ";

            $a_params = array();
            $a_param_type = array();
            $a_bind_params = array();
            $query = array();

            foreach ($searchItems as $key => $value) {
                switch ($key) {
                    case 'client_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cam.client_id = ? ";
                        break;
                    case 'client_master_id':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = 'cam.client_master_id = ? ';
                        break;
                    case 'is_active':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "cam.is_active = ?";
                        break;
                    case 'is_active1':
                        $a_param_type[] = 'i';
                        $a_bind_params[] = $value;
                        $query[] = "rcam.is_active = ?";
                        break;
                    default:
                        break;
                }
            }
            $sql .= implode(' AND ', $query);
            $param_type = '';
            $n = count($a_param_type);
            for ($i = 0; $i < $n; $i++) {
                $param_type .= $a_param_type[$i];
            }
            $a_params[] = &$param_type;
            for ($i = 0; $i < $n; $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            if ($stmt = $this->conn->prepare($sql)) {
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                $stmt->execute();
                $stmt->store_result();
                if ($stmt->num_rows > 0) {
                    $goAhead = true;
                    $aReportSource = array();
                    $stmt->bind_result($report_client_app_map_id, $report_id, $client_application_map_id, $report_name, $client_id,
                        $application_id, $application_name);
                    while ($result = $stmt->fetch()) {
                        $tmp = array();
                        $tmp["report_id"] = $report_id;
                        $tmp["report_name"] = $report_name;
                        $tmp["client_application_map_id"] = $client_application_map_id;
                        $tmp["report_client_app_map_id"] = $report_client_app_map_id;
                        $tmp["client_id"] = $client_id;
                        $response["reportClientApplicationMapDetails"][] = $tmp;
                    }
                    $response["error"] = false;
                    $response["message"] = RECORD_FOUND;
                } else {
                    $response["error"] = true;
                    $response["message"] = NO_RECORD_FOUND;
                }
            } else {
                $response["error"] = true;
                $response["message"] = QUERY_EXCEPTION;
            }
            $aReportSource = null;
            return $response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getAllReportApplicationDetails($searchParams)
    {
        try {
            $response = array();
            $newResponse = array();
            $abc = array();
            $objAppln = new report();
            $is_active = 1;
            $response = $objAppln->getAllReports($searchItems);
            if ($response['error'] == false) {
                $newResponse["error"] = false;
                foreach ($response['reportDetails'] as $k => $v) {
                    $newResponse['reportClientApplicationMapDetails'][] = $v;
                }
            }
            return $newResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * close the database connection
     */
    public function __destruct()
    {
        // close the database connection
        $this->db->closeconnection();
    }

}
