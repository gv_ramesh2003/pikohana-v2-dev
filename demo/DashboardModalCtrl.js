﻿'use strict';

app.controller('DashboardModalCtrl', ['$scope', '$uibModalInstance', '$uibModalStack', '$timeout', 'id',
  function ($scope, $uibModalInstance, $uibModalStack, $timeout, id) {
      var me = this;
      angular.extend(me, {
          init: function () {
              me.initScope();
          },
          initScope: function () {

              $scope.id = id;

              if (id == 1) {
                  $scope.modalTitle = 'Revenue';
                  $scope.modalContent = '<p>Revenue is the amount of money that a company receives during a specific period, including discounts and deductions for returned merchandise. It is the "top line" or "gross income" figure from which costs are subtracted to determine net income.</p>' +
                                        '<p>There are different ways of calculating revenue, depending on the accounting method a business employs. Accrual accounting will include sales made on credit as revenue, if the goods or services have been delivered to the customer. It is therefore necessary to check the cash flow statement to assess how efficiently a company collects the money it is owed.</p>'+
                                        '<p>' +
                                        'YTD Target vs. YTD Actual: This is the target (i.e., budget) for the year-to-date (‘YTD’) vs. actual results for the YTD.' +
                                        '</p>';
              }
              else if (id == 2) {
                  $scope.modalTitle = 'Expenses';
                  $scope.modalContent = '<p>Expenses include Cost of Sales and Operating Expenses.</p>'+
                                        '<p>'+
                                        'Cost of Sales (COS) are the direct costs attributable to the production and/or delivery of goods and services sold by a company. This amount includes the cost of the materials used in creating the good along with the direct labour costs used to produce the good or deliver the services. It excludes indirect expenses such as distribution costs and sales force costs. COS appears on the income statement and can be deducted from revenue to calculate a company\'s gross margin.' +
                                        '</p>'+
                                        '<p>'+
                                        'An operating expense is an expense a business incurs through its normal business operations. Often abbreviated as OPEX, operating expenses include rent, equipment, inventory costs, marketing, payroll, insurance and funds allocated toward research and development. One of the typical responsibilities that management must contend with is determining how low operating expenses can be reduced without significantly affecting a firm\'s ability to compete with its competitors.' +
                                        '</p>';
              }
              else if (id == 3) {
                  $scope.modalTitle = 'EBITDA';
                  $scope.modalContent = '<p>'+
                                        'EBITDA = Earnings Before Interest, Taxes, Depreciation and Amortization'+
                                        '</p>' +
                                        '<p>' +
                                        'The formula for EBITDA is: EBITDA = Net Profit + Interest +Taxes + Depreciation + Amortization'+
                                        '</p>'+
                                        '<p>'+
                                        'EBITDA is essentially net income with interest, taxes, depreciation and amortization added back to it. EBITDA can be used to analyse and compare profitability between companies and industries because it eliminates the effects of financing and accounting decisions. EBITDA is often used in valuation ratios and compared to enterprise value and revenue.'+
                                        '</p>';
              }
              else if (id == 4) {
                  $scope.modalTitle = 'Revenue';
                  $scope.modalContent = '<p>Revenue is the amount of money that a company receives during a specific period, including discounts and deductions for returned merchandise. It is the "top line" or "gross income" figure from which costs are subtracted to determine net income.</p>';
                                        '<p>';
                                        'YTD Target vs. YTD Actual';
                                        'This is the target (i.e., budget) for the year-to-date (‘YTD’) vs. actual results for the YTD.';
                                        '</p>';
              }
              else if (id == 5) {
                  $scope.modalTitle = 'Operating Expenses';
                  $scope.modalContent = '<p>Operating Expenses</p>';
              }
              else if (id == 6) {
                  $scope.modalTitle = 'Total Employee Compensation';
                  $scope.modalContent = '<p>'+
                                        'Total Employee Compensation refers to the total compensation received by an organization, which includes not only the team’s base salaries but bonuses, employee and employer payroll taxes, staff welfare and other forms of compensation'+
                                        '</p>';
              }
              else if (id == 7) {
                  $scope.modalTitle = 'EBITDA';
                  $scope.modalContent = '<p>EBITDA = Earnings Before Interest, Taxes, Depreciation and Amortization</p>' +
                                      '<p>The formula for EBITDA is: EBITDA = Net Profit + Interest +Taxes + Depreciation + Amortization</p>' +
                                      '<p>'+
                                      'EBITDA is essentially net income with interest, taxes, depreciation and amortization added back to it. EBITDA can be used to analyse and compare profitability between companies and industries because it eliminates the effects of financing and accounting decisions. EBITDA is often used in valuation ratios and compared to enterprise value and revenue.'+
                                      '</p>';
              }
              else if (id == 8) {
                  $scope.modalTitle = 'Days Sales Outstanding (DSO)';
                  $scope.modalContent = '<p>'+
                                        'Days sales outstanding (DSO) is a measure of the average number of days that a company takes to collect revenue after a sale has been made. DSO is often determined on a monthly, quarterly or annual basis and can be calculated by dividing the amount of accounts receivable during a given period by the total value of credit sales during the same period, and multiplying the result by the number of days in the period measured.' +
                                        '</p>'+
                                        '<p>'+
                                        'The formula for calculating days sales outstanding can be represented with the following formula:'+
                                        '</p>'+
                                        '<p>Average AR/(Sales for Period/90 days) = DSO</p>'+
                                        '<p>'+
                                        'A low DSO value means that it takes a company fewer days to collect its accounts receivable. A high DSO number shows that a company is selling its product to customers on credit and taking longer to collect money.'+
                                        '</p>'+
                                        '<p>'+
                                        'Days sales outstanding is also often referred to as “days receivables” and is an element of the cash conversion cycle.'+
                                        '</p>';
              }
              else if (id == 9) {
                  $scope.modalTitle = 'Days Payables Outstanding (DPO)';
                  $scope.modalContent = '<p>'+
                                        'Days payable outstanding (DPO) is a company\'s average payable period. Days payable outstanding tells how long it takes a company to pay its invoices from trade creditors, such as suppliers. DPO is typically looked at either quarterly or yearly.'+
                                        '</p>'+
                                        '<p><strong>Average AP/[(Cost of Sales + Total OPEX)/90 days] = DPO</strong></p>'+
                                        '<p>'+
                                        'Companies must strike a delicate balance with DPO. The longer they take to pay their creditors, the more money the company has on hand, which is good for working capital and free cash flow. But if the company takes too long to pay its creditors, the creditors will be unhappy. They may refuse to extend credit in the future, or they may offer less favourable terms. Also, because some creditors give companies a discount for timely payments, the company may be paying more than it needs to for its supplies. If cash is tight, however, the cost of increasing DPO may be less than the cost of foregoing that cash earlier and having to borrow the shortfall to continue operations.'+
                                        '</p>'+
                                        '<p>'+
                                        'Most companies’ DPO is about 30, meaning that it takes them about a month to pay their vendors. DPO can vary by industry, and a company can compare its DPO to the industry average to see if it is paying its vendors too quickly or too slowly.' +
                                        '</p>';
              }
              else if (id == 10) {
                  $scope.modalTitle = 'Cash Conversion Cycle (CCC)';
                  $scope.modalContent = '<p>'+
                                        'The cash conversion cycle (CCC) is a metric that expresses the length of time, in days, that it takes for a company to convert resource inputs into cash flows. The cash conversion cycle attempts to measure the amount of time each net input dollar is tied up in the production and sales process before it is converted into cash through sales to customers. This metric looks at the amount of time needed to sell product or services, the amount of time needed to collect receivables and the length of time the company is afforded to pay its bills without incurring penalties.'+
                                        '</p>';
              }
              else if (id == 11) {
                  $scope.modalTitle = 'Total Operating Runway';
                  $scope.modalContent = '<p>'+
                                        'A company\'s burn rate is also used as a measuring stick for its operating runway, the amount of time the company has before it runs out of money. So, if a company has $1 million in the bank, and it spends $100,000 a month, its burn rate would be $100,000 and its runway would be 10 months, derived as: ($1,000,000) / ($100,000) = 10.'+
                                        '</p>';
              }
              else if (id == 12) {
                  $scope.modalTitle = 'Current Ratio';
                  $scope.modalContent = '<p>'+
                                        'The current ratio is a liquidity ratio that measures a company\'s ability to pay short-term and long-term obligations. To gauge this ability, the current ratio considers the current total assets of a company (both liquid and illiquid) relative to that company’s current total liabilities.'+
                                        '</p>'+
                                        '<p>'+
                                        'The formula for calculating a company’s current ratio, then, is:'+
                                        '</p>'+
                                        '<p><strong>Current Ratio = Current Assets / Current Liabilities</strong></p>'+
                                        '<p>'+
                                        'The current ratio is called “current” because, unlike some other liquidity ratios, it incorporates all current assets and liabilities.'+
                                        '</p>'+
                                        '<p>'+
                                        'The current ratio is also known as the working capital ratio and is mainly used to give an idea of the company\'s ability to pay back its liabilities (debt and accounts payable) with its assets (cash, marketable securities, inventory, accounts receivable). As such, current ratio can be used to take a rough measurement of a company’s financial health. The higher the current ratio, the more capable the company is of paying its obligations, as it has a larger proportion of asset value relative to the value of its liabilities.'+
                                        '</p>';
              }
              else if (id == 13) {
                  $scope.modalTitle = 'Short Term Working Capital';
                  $scope.modalContent = '<p>'+
                                        'Short Term Working capital is a measure of the company\'s short-term financial health. Working capital is calculated as:'+
                                        '</p>'+
                                        '<p>'+
                                        '<strong>Working Capital = Current Assets - Current Liabilities</strong>'+
                                        '</p>'+
                                        '<p>'+
                                        'The working capital ratio (Current Assets/Current Liabilities) indicates whether a company has enough short-term assets to cover its short-term debt. Anything below 1 indicates negative W/C (working capital). While anything over 2 means that the company is not investing excess assets. Most believe that a ratio between 1.2 and 2.0 is sufficient.  Also known as "net working capital".'+
                                        '</p>'+
                                        '<p>'+
                                        'If a company\'s current assets do not exceed its current liabilities, then it may run into trouble paying back creditors in the short term.'+
                                        '</p>';
              }
              else if (id == 14) {
                  $scope.modalTitle = 'TOP 10 EXPENSES';
                  $scope.modalContent = '<p>' +
                                        'These are the Top 10 expense line items for the year-to-date.' +
                                        '</p>' +
                                        '<p>' +
                                        'The percentages are calculated by dividing each line item by the YTD Actual Expenses, as stated in Section 1 of the dashboard.' +
                                        '</p>';
              }
          }
      }).init();

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  }]);